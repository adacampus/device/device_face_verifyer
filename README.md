# 人脸审核APP 版本更新内容

------
### app v1.0.0.00 2020-11-19
- app file md5: 35f61dab8f34cadb0916511e36391041
- app file name: ada_face_verifyer_release_1.0.0.00_201119-1645.apk
- device_lib git commit id: 44ed6c4bfc2fc6d2dae528655903fac853101626
- 只用于人脸审核的app
- app打包的签名使用班牌app同一个
