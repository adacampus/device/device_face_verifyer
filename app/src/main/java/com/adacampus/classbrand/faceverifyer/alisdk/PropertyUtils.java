package com.adacampus.classbrand.faceverifyer.alisdk;

import com.adacampus.classbrand.library.utils.AdaLog;
import com.adacampus.classbrand.faceverifyer.utils.Key;
import com.aliyun.alink.linkkit.api.LinkKit;
import com.aliyun.alink.linksdk.tmp.device.payload.ValueWrapper;
import com.aliyun.alink.linksdk.tmp.listener.IPublishResourceListener;
import com.aliyun.alink.linksdk.tools.AError;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class PropertyUtils {


    /**
     * 有则返回，没有则创建一个默认，保证Property文件一定存在
     *
     * @return
     */
    public static void checkProperty() {
        String path = AliSdkUtils.getPropertyFilePath();
        File file = new File(path);
        if (!file.exists()) {
            createPropertyFile(file);
        }
    }


    /**
     * @return
     * @throws IOException
     * @desc 将属性文件转成FaceDoorProperty类
     */
    public static FaceDoorProperty getProperty() {
        Gson gson = new Gson();
        FileInputStream is = null;
        StringBuilder stringBuilder = null;
        try {
            is = new FileInputStream(AliSdkUtils.getPropertyFilePath());
            InputStreamReader inputStreamReader = new InputStreamReader(is);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line;
            stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            bufferedReader.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (stringBuilder == null) {
            return null;
        }

        return gson.fromJson(stringBuilder.toString(), FaceDoorProperty.class);
    }


    public static void writeProperty(FaceDoorProperty property) {
        Gson gson = new Gson();
        // 若文件不存在，则创建
        try {
            String jsonProperty = gson.toJson(property, FaceDoorProperty.class);
            FileOutputStream fos = new FileOutputStream(AliSdkUtils.getPropertyFilePath());
            fos.write(jsonProperty.getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String readProperty() {
        FileInputStream is = null;
        StringBuilder stringBuilder = null;
        try {
            is = new FileInputStream(AliSdkUtils.getPropertyFilePath());
            InputStreamReader inputStreamReader = new InputStreamReader(is);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line;
            stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            bufferedReader.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }


    public static Map<String, ValueWrapper> getPropertyMap(String propertyStr) {
        FaceDoorProperty faceDoorProperty = new Gson().fromJson(propertyStr, FaceDoorProperty.class);
        Map<String, ValueWrapper> map = new HashMap<>();
        map.put("OnMatchThreshOld", new ValueWrapper.DoubleValueWrapper(faceDoorProperty.getOnMatchThreshOld()));
        map.put("FaceSetMD5Sign", new ValueWrapper.StringValueWrapper(faceDoorProperty.getFaceSetMD5Sign()));
        map.put("FaceSetID", new ValueWrapper.StringValueWrapper(faceDoorProperty.getFaceSetID()));
        map.put("FaceSetSize", new ValueWrapper.IntValueWrapper(faceDoorProperty.getFaceSetSize()));
        map.put("FaceSetAlgorithmVersion", new ValueWrapper.StringValueWrapper(faceDoorProperty.getFaceSetAlgorithmVersion()));
        map.put("FaceSetPicStoreAbility", new ValueWrapper.IntValueWrapper(faceDoorProperty.getFaceSetPicStoreAbility()));
        return map;
    }


    public static void createPropertyFile(File propertyFile) {
        try {
            // /storage/emulated/0/Android/data/com.alibaba.security.rp.verifyclient.debug/cache/Property
            propertyFile.createNewFile();

            // 初始化设备属性
            FaceDoorProperty faceDoorProperty = new FaceDoorProperty();
            faceDoorProperty.setOnMatchThreshOld(0.27);
            faceDoorProperty.setFaceSetPicStoreAbility(270);
            faceDoorProperty.setFaceSetSize(27);
            faceDoorProperty.setFaceSetMD5Sign("270270270");
            faceDoorProperty.setFaceSetAlgorithmVersion("2.7.0");
            faceDoorProperty.setFaceSetID("LY");
            faceDoorProperty.setAddedFaceSetName("");

            String jsonProperty = new Gson().toJson(faceDoorProperty, FaceDoorProperty.class);
            FileOutputStream fos = new FileOutputStream(AliSdkUtils.getPropertyFilePath());
            fos.write(jsonProperty.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void uploadProperty() {
        checkProperty();
        String propertyStr = PropertyUtils.readProperty();
        Map<String, ValueWrapper> propertyMap = getPropertyMap(propertyStr);
        linkKitUploadProperty(propertyMap);
    }

    public static void linkKitUploadProperty(Map<String, ValueWrapper> propertyMap) {
        LinkKit.getInstance().getDeviceThing().thingPropertyPost(propertyMap, new IPublishResourceListener() {
            @Override
            public void onSuccess(String resID, Object object) {
                AdaLog.d(Key.LOG, AliSdkManager.Tag, "uploadPropertyFile", "onSuccess" + "resID:" + resID);
            }

            @Override
            public void onError(String resId, AError aError) {
                AdaLog.d(Key.LOG, AliSdkManager.Tag, "uploadPropertyFile", "onError" + "resID:" + resId);
            }
        });
    }


}
