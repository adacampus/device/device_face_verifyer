package com.adacampus.classbrand.faceverifyer.alisdk;

import java.io.Serializable;

public class AliIot implements Serializable {

    public String id;
    public String secret;
    public String product_key;
    public boolean face_license;
}
