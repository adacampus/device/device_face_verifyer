package com.adacampus.classbrand.faceverifyer.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.adacampus.classbrand.library.base.BaseApp;

/**
 * Created by Jeffrey on 16/12/9.
 */

public class SharedPreferencesUtil {


    public static String ETAG_STUDENT_FACE_LIST = "etag_student_face_list";



    public static void saveAddSuccessStudentFaceList(String json) {
        SharedPreferences sp = BaseApp.getApp().getSharedPreferences("face_list", Context.MODE_PRIVATE);
        sp.edit().putString("add_success_student_face_list", json).apply();
    }

    public static String getAddSuccessStudentFaceList() {
        SharedPreferences sp = BaseApp.getApp().getSharedPreferences("face_list", Context.MODE_PRIVATE);
        return sp.getString("add_success_student_face_list", "");
    }

    public static void saveAddFailureStudentFaceList(String json) {
        SharedPreferences sp = BaseApp.getApp().getSharedPreferences("face_list", Context.MODE_PRIVATE);
        sp.edit().putString("add_failure_student_face_list", json).apply();
    }

    public static String getAddFailureStudentFaceList() {
        SharedPreferences sp = BaseApp.getApp().getSharedPreferences("face_list", Context.MODE_PRIVATE);
        return sp.getString("add_failure_student_face_list", "");
    }


    public static void saveFaceSyncProgress(float progress) {
        SharedPreferences sp = BaseApp.getApp().getSharedPreferences("face_data", Context.MODE_PRIVATE);
        sp.edit().putFloat("last_sync_progress", progress).apply();
    }

    /**
     * 从来没有保存 -1.0f
     * 保存过一次或以上 0.0f
     * 其他正常状态 0.0f ~ 1.0f
     *
     * @return
     */
    public static float getFaceSyncProgress() {
        SharedPreferences sp = BaseApp.getApp().getSharedPreferences("face_data", Context.MODE_PRIVATE);
        return sp.getFloat("last_sync_progress", -1.0f);
    }


    public static boolean getNeedUpdateVerifySdk() {
        SharedPreferences sp = BaseApp.getApp().getSharedPreferences("verify_sdk", Context.MODE_PRIVATE);
        return sp.getBoolean("needUpdate", true);
    }

    public static void setNeedUpdateVerifySdk(boolean needUpdate) {
        SharedPreferences sp = BaseApp.getApp().getSharedPreferences("verify_sdk", Context.MODE_PRIVATE);
        sp.edit().putBoolean("needUpdate", needUpdate).apply();
    }

    public static void saveVerifySdkInitSuccessTimeStamp(long timeStamp) {
        SharedPreferences sp = BaseApp.getApp().getSharedPreferences("verify_sdk", Context.MODE_PRIVATE);
        sp.edit().putLong("initSuccessTimeStamp", timeStamp).apply();
    }

    public static long getVerifySdkInitSuccessTimeStamp() {
        SharedPreferences sp = BaseApp.getApp().getSharedPreferences("verify_sdk", Context.MODE_PRIVATE);
        return sp.getLong("initSuccessTimeStamp", 0);
    }

    public static void setATConfig(boolean config) {
        SharedPreferences sp = BaseApp.getApp().getSharedPreferences("config", Context.MODE_PRIVATE);
        sp.edit().putBoolean("at_config", config).apply();
    }

    public static boolean getATConfig() {
        SharedPreferences sp = BaseApp.getApp().getSharedPreferences("config", Context.MODE_PRIVATE);
        return sp.getBoolean("at_config", false);
    }


}
