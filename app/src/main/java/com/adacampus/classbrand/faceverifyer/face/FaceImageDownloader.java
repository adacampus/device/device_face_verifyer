package com.adacampus.classbrand.faceverifyer.face;

import android.content.Context;
import android.text.format.Formatter;

import com.adacampus.classbrand.faceverifyer.utils.MsgEvent;
import com.adacampus.classbrand.library.base.BaseApp;
import com.adacampus.classbrand.library.utils.AdaLog;
import com.adacampus.classbrand.library.utils.FilePath;
import com.adacampus.classbrand.faceverifyer.utils.Key;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Priority;
import com.lzy.okgo.model.Progress;
import com.lzy.okgo.request.GetRequest;
import com.lzy.okserver.OkDownload;
import com.lzy.okserver.download.DownloadListener;
import com.lzy.okserver.download.DownloadTask;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.text.NumberFormat;

public class FaceImageDownloader {

    private Context mContext;
    private NumberFormat mNumberFormat;

    private OnFaceImageDownloadCallback mFaceImageDownloadCallback;

    public FaceImageDownloader(Context context) {
        mContext = context;
        mNumberFormat = NumberFormat.getPercentInstance();
        mNumberFormat.setMinimumFractionDigits(2);
        mNumberFormat.setMinimumIntegerDigits(0);
        mNumberFormat.setMaximumFractionDigits(2);
        mNumberFormat.setMaximumIntegerDigits(3);
    }

    public void download(Face face) {
        if (OkDownload.getInstance().hasTask(face.image_url)) {
            OkDownload.getInstance().getTask(face.image_url).pause();
            OkDownload.getInstance().removeTask(face.image_url);
        }
        GetRequest<File> request = OkGo.<File>get(face.image_url);
        DownloadTask downloadTask = OkDownload.request(face.image_url, request)
                .priority(Priority.UI_TOP)
                .fileName(face.id)
                .folder(FilePath.getFaceDataPath());
        downloadTask.register(new FaceImageDownloadListener(face));
        //源码貌似没有做到应用退出可以断点续传，但是可以不退出应用继续下载
        if (downloadTask.progress.currentSize <= 0) { //第一次下载
            downloadTask.save();
        }
        downloadTask.start();
    }

    private class FaceImageDownloadListener extends DownloadListener {

        private Face mFace;
        private int mLogCount = 0;

        public FaceImageDownloadListener(Face face) {
            super(face);
            mFace = face;
        }

        @Override
        public void onStart(Progress progress) {
            EventBus.getDefault().post(new MsgEvent("开始下载:" + mFace.face_ident));
            AdaLog.d(Key.LOG, FaceImageDownloadListener.this, "onStart", "user_id:" + mFace.id + " face_id:" + mFace.id + "\nprogress:\n" + progress.toString());
        }

        @Override
        public void onProgress(Progress progress) {
            float percentF = (float) progress.currentSize / progress.totalSize;
            String percentStr = mNumberFormat.format(percentF);
            String speed = Formatter.formatFileSize(mContext, progress.speed);
            BaseApp.showShortToast("正在下载" + mFace.face_ident + ":" + percentStr + "，" + String.format("速度:%s/s", speed));
            if (mLogCount % 2 == 0) {
                AdaLog.d(Key.LOG, FaceImageDownloadListener.this, "onProgress", "user_id:" + mFace.id + " face_id:" + mFace.face_ident + "\nprogress:\n" + progress.toString());
            }
            mLogCount++;
        }

        @Override
        public void onError(Progress progress) {
            EventBus.getDefault().post(new MsgEvent("下载失败:" + mFace.face_ident));
            AdaLog.e(Key.LOG, FaceImageDownloadListener.this, "onError", "user_id:" + mFace.id + " face_id:" + mFace.face_ident + "\nprogress:\n" + progress.toString());
            if (mFaceImageDownloadCallback != null) {
                mFaceImageDownloadCallback.onLoadFaceImageFailure(mFace, progress);
            }
        }

        @Override
        public void onFinish(File file, Progress progress) {
            EventBus.getDefault().post(new MsgEvent("下载完成:" + mFace.face_ident));
            AdaLog.d(Key.LOG, FaceImageDownloadListener.this, "onFinish", "file:" + file.getAbsolutePath() + "\n" + "user_id:" + mFace.id + " face_id:" + mFace.face_ident + "\nprogress:\n" + progress.toString());
            if (mFaceImageDownloadCallback != null) {
                mFaceImageDownloadCallback.onLoadFaceImageSuccessful(mFace, file);
            }
        }

        @Override
        public void onRemove(Progress progress) {
            AdaLog.d(Key.LOG, FaceImageDownloadListener.this, "onRemove:", "user_id:" + mFace.id + " face_id:" + mFace.face_ident + "\nprogress:" + progress.toString());
        }
    }


    public void setFaceImageDownloadCallback(OnFaceImageDownloadCallback callback) {
        mFaceImageDownloadCallback = callback;
    }

    public interface OnFaceImageDownloadCallback {

        public void onLoadFaceImageFailure(Face face, Progress progress);

        public void onLoadFaceImageSuccessful(Face face, File file);

    }
}
