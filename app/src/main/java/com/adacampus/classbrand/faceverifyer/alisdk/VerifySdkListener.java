package com.adacampus.classbrand.faceverifyer.alisdk;

import com.alibaba.security.rp.verifysdk.service.VerifySDKManager;

/**
 * 用于精简使用回调时不必实现其他没用方法
 */
public abstract class VerifySdkListener implements VerifySDKManager.VerifyLibEventListener {
    @Override
    public void onSingleUserLibUpdate(final String id, final int errorCode) {

    }

    @Override
    public void onBatchUserLibUpdate(final int errorCode) {

    }

    @Override
    public void onUserLibLoaded(final int errorCode) {

    }

    @Override
    public void onUserLibEmpty(final int errorCode) {

    }
}
