package com.adacampus.classbrand.faceverifyer;


import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adacampus.classbrand.faceverifyer.alisdk.AliSdkManager;
import com.adacampus.classbrand.faceverifyer.alisdk.OnAliSDKListener;
import com.adacampus.classbrand.faceverifyer.utils.CommonEvent;
import com.adacampus.classbrand.faceverifyer.utils.Key;
import com.adacampus.classbrand.library.base.BaseActivity;
import com.adacampus.classbrand.library.base.BaseApp;
import com.adacampus.classbrand.library.net.hearbeat.NetworkConnectionHelper;
import com.adacampus.classbrand.library.net.mq.MQHelper;
import com.adacampus.classbrand.library.utils.AdaLog;
import com.adacampus.classbrand.library.utils.AppUtils;
import com.adacampus.classbrand.faceverifyer.net.SysRetrofitHelper;
import com.adacampus.classbrand.faceverifyer.utils.MsgEvent;
import com.adacampus.classbrand.library.utils.device.ChipIdHelper;
import com.adacampus.classbrand.library.utils.device.DeviceSNK;
import com.aliyun.alink.linksdk.cmp.core.base.ConnectState;
import com.taro.headerrecycle.adapter.SimpleRecycleAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class MainActivity extends BaseActivity implements
        OnAliSDKListener,
        NetworkConnectionHelper.OnCountChangedListener,
        NetworkConnectionHelper.OnStatusListener {

    private RecyclerView mRecyclerView;
    private SimpleRecycleAdapter mAdapter;
    private MsgOption mMsgOption;
    private List<String> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.recyclerView);
        MainApp.postUIThreadDelay(new Runnable() {
            @Override
            public void run() {
                init();
            }
        }, 1000);
    }


    @Override
    protected void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();
    }


    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final MsgEvent event) {
        String msg = changeMsgColor(event.mMsg);
        mList.add(getTime() + ": " + msg);
        if (mList.size() >= 3000) {
            mList.remove(0);
        }
        mAdapter.setItemList(mList);
        mAdapter.notifyDataSetChanged();
        mRecyclerView.smoothScrollToPosition(mAdapter.getItemList().size() - 1);
        AdaLog.d(Key.LOG, MainActivity.this, "onMessageEvent", "msg:" + event.mMsg);
    }

    private String changeMsgColor(String msg) {
        if (msg.contains("成功")) {
            msg = msg.replaceAll("成功", "<font color='#00796d'>成功</font>");
        }
        if (msg.contains("失败")) {
            msg = msg.replaceAll("失败", "<font color='#dd191d'>失败</font>");
        }

        if (msg.contains("完成")) {
            msg = msg.replaceAll("完成", "<font color='#00796d'>完成</font>");
        }
        return msg;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final CommonEvent event) {

    }


    @Override
    public void onSdkAuth(boolean successful) {
        String msg = null;
        if (successful) {
            msg = "sdk授权验证成功";
        } else {
            msg = "sdk授权验证失败";
        }
        EventBus.getDefault().post(new MsgEvent(msg));
    }

    @Override
    public void onLocalFaceLoading(boolean loading) {
        String msg = null;
        if (loading) {
            msg = "本地人脸库开始加载";
        } else {
            msg = "本地人脸库加载完成";
            MQHelper.getInstance().init("", 0);
            MQHelper.getInstance().start();
        }
        EventBus.getDefault().post(new MsgEvent(msg));
    }

    @Override
    public void onIotConnectState(ConnectState connectState) {

    }

    @Override
    public void onSyncFaceProgress(float progress) {
    }

    @Override
    public void onFirstCallback(boolean connected) {

    }

    @Override
    public void onChange(boolean connected) {
        String state = connected ? "已连接" : "已断开";
        ((TextView) findViewById(R.id.tv_connected)).setText("连接状态:" + state);
    }

    @Override
    public void onFirstNetworkGood() {

    }

    @Override
    public void onNetworkGood() {

    }

    @Override
    public void onConnectStateCountChanged(long successfulCount, long failedCount) {
        ((TextView) findViewById(R.id.tv_successfulCount)).setText("连接成功次数:" + successfulCount);
        ((TextView) findViewById(R.id.tv_failedCount)).setText("连接失败次数:" + failedCount);
    }

    @Override
    public void onRequestTimeChange(long sigleRequestTime, long maxRequestTime, long minRequestTime) {
        ((TextView) findViewById(R.id.tv_sigleRequestTime)).setText("当前连接响应速度:" + sigleRequestTime + "毫秒");
        ((TextView) findViewById(R.id.tv_maxRequestTime)).setText("最大连接响应速度:" + maxRequestTime + "毫秒");
        ((TextView) findViewById(R.id.tv_minRequestTime)).setText("最小连接响应速度:" + minRequestTime + "毫秒");
    }

    private void init() {
        mMsgOption = new MsgOption();
        mAdapter = new SimpleRecycleAdapter(this, mMsgOption, mList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);


        ChipIdHelper chipIdHelper = new ChipIdHelper();
        DeviceSNK deviceSNK = chipIdHelper.read(this);
        ((TextView) findViewById(R.id.tv_sn)).setText("序列号:" + deviceSNK.mSN);
        ((TextView) findViewById(R.id.tv_chipid)).setText("chip id:" + chipIdHelper.getChipIdCompat());
        SysRetrofitHelper.getInstance().init(BuildConfig.API_URI);
        NetworkConnectionHelper.getInstance().action();
        NetworkConnectionHelper.getInstance().setOnStatusListener(this);
        NetworkConnectionHelper.getInstance().setOnCountChangedListener(this);
        initAliSdk();
    }

    public void initAliSdk() {
        if (!AliSdkManager.getInstance().isInited()) {
            AliSdkManager.getInstance().init(this, this);
        }
        if (!AliSdkManager.getInstance().isRunning()) {
            AliSdkManager.getInstance().run();
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_SETTINGS) {
            AppUtils.launchSettingApp(this);
        }
        return super.onKeyDown(keyCode, event);
    }

    private long lastBackTime;

    @Override
    public void onBackPressed() {
        long nowTime = System.currentTimeMillis();
        if (nowTime - lastBackTime > 2000) {
            BaseApp.showShortToast("再按一下退出！");
        } else {
            AppUtils.stopApp(MainApp.getApp().getMainAppId());
        }
        lastBackTime = nowTime;
    }

    private String getTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        String time = sdf.format(Calendar.getInstance().getTime());
        return time;
    }
}

