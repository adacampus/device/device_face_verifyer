package com.adacampus.classbrand.faceverifyer.net;


import com.adacampus.classbrand.library.utils.ListUtils;

import java.util.List;


public class ListDataParser<T> implements IDataParser<T> {

    public List<T> mList;
    public int mIndex = -1;

    public void setData(List<T> t) {
        mIndex = -1;
        mList = t;
    }

    public List<T> getData() {
        return mList;
    }

    @Override
    public T getNext() {
        if (ListUtils.isEmpty(mList)) {
            return null;
        }
        mIndex++;
        if (mIndex > mList.size() - 1) {
            return null;
        }
        T data = mList.get(mIndex);
        return data;
    }


    @Override
    public T getPrevious() {
        if (ListUtils.isEmpty(mList)) {
            return null;
        }
        mIndex--;
        if (mIndex < 0) {
            return null;
        }
        T data = mList.get(mIndex);
        return data;
    }


    @Override
    public T getCurrent() {
        if (ListUtils.isEmpty(mList)) {
            return null;
        }
        if (mList.size() - 1 < mIndex || 0 > mIndex) {
            return null;
        }
        T data = mList.get(mIndex);
        return data;
    }

    @Override
    public boolean isEmpty() {
        if (ListUtils.isEmpty(mList)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean hasNext() {
        if (ListUtils.isEmpty(mList)) {
            return false;
        }
        if (mIndex + 1 < mList.size()) {
            return true;
        }
        return false;
    }

    public boolean hasCurrent() {
        if (ListUtils.isEmpty(mList)) {
            return false;
        }
        if (mIndex != -1 && mIndex + 1 < mList.size()) {
            return true;
        }
        return false;
    }
}
