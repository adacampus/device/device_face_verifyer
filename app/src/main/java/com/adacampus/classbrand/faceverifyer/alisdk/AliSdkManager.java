package com.adacampus.classbrand.faceverifyer.alisdk;


import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;

import com.adacampus.classbrand.faceverifyer.BuildConfig;
import com.adacampus.classbrand.faceverifyer.MainActivity;
import com.adacampus.classbrand.faceverifyer.MainApp;
import com.adacampus.classbrand.faceverifyer.face.verify.FaceDataVerifyer;
import com.adacampus.classbrand.faceverifyer.utils.CommonEvent;
import com.adacampus.classbrand.faceverifyer.utils.MsgEvent;
import com.adacampus.classbrand.faceverifyer.utils.Key;
import com.adacampus.classbrand.faceverifyer.utils.SharedPreferencesUtil;
import com.adacampus.classbrand.library.base.BaseApp;
import com.adacampus.classbrand.library.net.mq.MQHelper;
import com.adacampus.classbrand.library.utils.AdaLog;
import com.adacampus.classbrand.library.utils.AppUtils;
import com.adacampus.classbrand.library.utils.FileUtils;
import com.alibaba.security.rp.verifysdk.service.VerifySDKManager;
import com.alibaba.wireless.security.open.umid.IUMIDComponent;
import com.aliyun.alink.dm.api.DeviceInfo;
import com.aliyun.alink.dm.api.IoTApiClientConfig;
import com.aliyun.alink.h2.api.CompletableListener;
import com.aliyun.alink.h2.entity.Http2Response;
import com.aliyun.alink.linkkit.api.ILinkKitConnectListener;
import com.aliyun.alink.linkkit.api.IoTH2Config;
import com.aliyun.alink.linkkit.api.IoTMqttClientConfig;
import com.aliyun.alink.linkkit.api.LinkKit;
import com.aliyun.alink.linkkit.api.LinkKitInitParams;
import com.aliyun.alink.linksdk.cmp.connect.channel.MqttPublishRequest;
import com.aliyun.alink.linksdk.cmp.core.base.AMessage;
import com.aliyun.alink.linksdk.cmp.core.base.ARequest;
import com.aliyun.alink.linksdk.cmp.core.base.AResponse;
import com.aliyun.alink.linksdk.cmp.core.base.ConnectState;
import com.aliyun.alink.linksdk.cmp.core.listener.IConnectNotifyListener;
import com.aliyun.alink.linksdk.cmp.core.listener.IConnectSendListener;
import com.aliyun.alink.linksdk.tmp.api.OutputParams;
import com.aliyun.alink.linksdk.tmp.device.payload.ValueWrapper;
import com.aliyun.alink.linksdk.tmp.listener.IPublishResourceListener;
import com.aliyun.alink.linksdk.tools.AError;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Envelope;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;


/**
 * 阿里verify sdk 和 iot sdk 组件管理
 * http://gaic.alicdn.com/ztms/android-iot-device-sdk-api-reference-v1030/html/overview-summary.html
 * <p>
 * <p>
 * 1.AliSdkManager2.init
 * 2.AliSdkManager2.run
 */
public class AliSdkManager {

    public static final String Tag = "AliSdkManager";

    private static AliSdkManager mManager;
    private MainActivity mContext;
    private OnAliSDKListener mOnAliSDKListener;
    private boolean mInited = false;
    private boolean mRunning = false;
    private FaceDataVerifyer mFaceDataVerifyer;

    /**
     * 标记verifysdk尝试初始化，目的是解决未init时getLicenseContext内部context会null造成的报错，以后阿里解决这问题后可删除此参数
     */
    private boolean mVerifySdkTryInit = false;


    //VerifySDK
    private int mVerifySdkInitCode = -1;
    private String mToken;
    private String mEncrypt;
    private String mProductKey; // 产品类型
    private String mDeviceName;// 设备名称
    private String mDeviceSecret;// 设备密钥 ???
    private String mProductSecret;// 产品密钥


    //iot
    private boolean mLinkKitInit = false;
    private boolean mRegPushListener = false;
    public static final String EVENT_NEED_AUTH_VERIFY = "NeedAuthVerifySDK";
    private MQEventHandler mEventHandler;
    private ConnectState mConnectState = ConnectState.DISCONNECTED;

    //data
    private BaseH2Util mBaseH2Util = new BaseH2Util();
    private FaceDataResetEventHandler mFaceDataResetEventHandler;


    private class FaceDataResetEventHandler extends com.adacampus.classbrand.library.net.mq.EventHandler {
        public FaceDataResetEventHandler() {
            mEventKey = Key.KEY_EVENT_FACE_SET_RESET;
        }


        @Override
        public void handleEvent(JSONObject json, Channel channel, Envelope envelope) {
            EventBus.getDefault().post(new MsgEvent(Key.KEY_EVENT_FACE_SET_RESET));
            ack();
            clearAllFaceData();
        }
    }


    public static AliSdkManager getInstance() {
        if (mManager == null) {
            synchronized (AliSdkManager.class) {
                if (mManager == null) {
                    mManager = new AliSdkManager();
                }
            }
        }
        return mManager;
    }

    public boolean isInited() {
        return mInited;
    }

    public boolean isRunning() {
        return mRunning;
    }


    public void init(final MainActivity context, OnAliSDKListener onAliSDKListener) {
        mContext = context;
        mOnAliSDKListener = onAliSDKListener;
        mProductKey = BuildConfig.PRODUCT_KEY;
        mDeviceName = BuildConfig.DEVICE_NAME;
        mDeviceSecret = BuildConfig.DEVICE_SECRET;

        mProductSecret = null;
        String rrpcTopic = "/sys/" + mProductKey + "/" + mDeviceName + "/rrpc/request";
        mEventHandler = new MQEventHandler(context, rrpcTopic);
        mInited = true;
        mFaceDataVerifyer = new FaceDataVerifyer();
        MQHelper.getInstance().addCriticalEvnetHandler(mFaceDataVerifyer.getEventHandler());
        mFaceDataResetEventHandler = new FaceDataResetEventHandler();
        MQHelper.getInstance().addCriticalEvnetHandler(mFaceDataResetEventHandler);
        EventBus.getDefault().post(new MsgEvent("---init AliSdkManager---"));

    }


    private int initVerifySDK() {
        float matchThreshold = 0.4f;
        boolean recapCheck = false;
        VerifySDKManager.getInstance().setFaceMatchThreshold(matchThreshold);
        AdaLog.d(Key.LOG, Tag, "initVerifySDK", "setFaceMatchThreshold:" + matchThreshold);
        VerifySDKManager.getInstance().setNeedRecapCheck(recapCheck);
        AdaLog.d(Key.LOG, Tag, "initVerifySDK", "setNeedRecapCheck:" + recapCheck);
        if (mVerifySdkTryInit) { //有初始化过就需要release一次
            AdaLog.d(Key.LOG, Tag, "initVerifySDK", "release");
            VerifySDKManager.getInstance().release();
        }

        //可能存在初始化过程中时间被同步修改的情况,保存调用 VerifySDKManager.getInstance().init（）前的时间会比较准确
        long initTimeStamp = System.currentTimeMillis();
        int initCode = VerifySDKManager.getInstance().init(mContext);
        AdaLog.d(Key.LOG, Tag, "initVerifySDK", "initCode:" + initCode);
        EventBus.getDefault().post(new MsgEvent("initVerifySDK()." + "initCode:" + initCode));
        if (initCode == 0) {
            SharedPreferencesUtil.saveVerifySdkInitSuccessTimeStamp(initTimeStamp);
            AdaLog.d(Key.LOG, Tag, "initVerifySDK", "save init time stamp:" + initTimeStamp);
        }
        mVerifySdkTryInit = true;
        return initCode;
    }


    public void setLicense(String license) {
        AdaLog.d(Key.LOG, AliSdkManager.Tag, "setLicense", "license: " + license);
        EventBus.getDefault().post(new MsgEvent("导入授权license"));
        byte[] hexData = ByteUtils.hexToBytes(license);
        VerifySDKManager.getInstance().setLicense(hexData);

    }


    public int getVerifySdkInitCode() {
        return mVerifySdkInitCode;
    }


    private class RetryInitVerifySdkRunnable implements Runnable {

        @Override
        public void run() {
            if (mVerifySdkInitCode == 5010) {
                long lastInitTimeStamp = SharedPreferencesUtil.getVerifySdkInitSuccessTimeStamp();
                long nowTimeStamp = System.currentTimeMillis();
                long difference = lastInitTimeStamp - nowTimeStamp;
                AdaLog.d(Key.LOG, AliSdkManager.Tag, "RetryInitVerifySdkRunnable.run", "lastInitTimeStamp:" + lastInitTimeStamp + " - nowTimeStamp:" + nowTimeStamp + " = dif:" + difference);
                if (difference > 0) { //上次初始化成功比当前时间早
                    try {
                        AdaLog.d(Key.LOG, AliSdkManager.Tag, "RetryInitVerifySdkRunnable.run", "sleep:start");
                        Thread.sleep(difference);
                        AdaLog.d(Key.LOG, AliSdkManager.Tag, "RetryInitVerifySdkRunnable.run", "sleep:end");
                        retryInitVerifySdk();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {  //出现其他情况，尝试直接再重新初始化
                    retryInitVerifySdk();
                }
            }
        }
    }

    public void tryInitVeriy() {
        AdaLog.d(Key.LOG, AliSdkManager.Tag, "tryInitVeriy", "");
        mVerifySdkInitCode = initVerifySDK();  //第一次初始化
        if (mVerifySdkInitCode == 0) {
            mOnAliSDKListener.onSdkAuth(true);
            loadLib();
        } else if (mVerifySdkInitCode == 5010) {
            FaceDoorThreadPool.getInstance().getThreadPool().execute(new RetryInitVerifySdkRunnable());
            initVerifySdkError();
        } else {
            initVerifySdkError();
        }
    }

    public void retryInitVerifySdk() {
        AdaLog.d(Key.LOG, AliSdkManager.Tag, "retryInitVerifySdk", "");
        tryInitVeriy();
    }

    public void reInitVerifySdkError() {
        AdaLog.d(Key.LOG, AliSdkManager.Tag, "reInitVerifySdkError", "");
        initVerifySdkError();
    }

    private void initVerifySdkError() {
        AdaLog.d(Key.LOG, AliSdkManager.Tag, "initVerifySdkError", "");
        mOnAliSDKListener.onSdkAuth(false);
    }


    public void setFaceMatchThreshold(float param) {
        AdaLog.d(Key.LOG, AliSdkManager.Tag, "setFaceMatchThreshold", "faceMatchThreshold: " + param);
        VerifySDKManager.getInstance().setFaceMatchThreshold(param);
    }

    public void run() {
        FaceDoorThreadPool.getInstance().getThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                mRunning = true;
                tryInitVeriy();
                conectToIot();
            }
        });
    }


    private void conectToIot() {

        LinkKitInitParams params = new LinkKitInitParams();

        DeviceInfo deviceInfo = new DeviceInfo();
        if (!mVerifySdkTryInit) {   //此判断参见mVerifySdkTryInit变量定义
            initVerifySDK();
        }
        mToken = VerifySDKManager.getInstance().getSecurityToken(mContext, IUMIDComponent.ENVIRONMENT_ONLINE).getToken();
        mEncrypt = VerifySDKManager.getInstance().getLicenseContext(mToken).getEncryptData();
        deviceInfo.productKey = mProductKey;
        deviceInfo.deviceName = mDeviceName;
        deviceInfo.deviceSecret = mDeviceSecret;
        deviceInfo.productSecret = mProductSecret;

        Map<String, ValueWrapper> propertyValues = new HashMap<>(16);
        IoTApiClientConfig connectConfig = new IoTApiClientConfig();
        IoTMqttClientConfig mqttClientConfig = new IoTMqttClientConfig(mProductKey, mDeviceName, mDeviceSecret);
        mqttClientConfig.isCheckChannelRootCrt = true;

        IoTH2Config ioTH2Config = new IoTH2Config();
        ioTH2Config.clientId = "client-id";
        ioTH2Config.endPoint = "https://" + mProductKey + ioTH2Config.endPoint;// 线上环境

        params.deviceInfo = deviceInfo;
        params.connectConfig = connectConfig;
        params.propertyValues = propertyValues;
        params.mqttClientConfig = mqttClientConfig;
        params.iotH2InitParams = ioTH2Config;

        /**
         * 设备初始化建联
         * onError 初始化建联失败，需要用户重试初始化。如因网络问题导致初始化失败。
         * onInitDone 初始化成功
         * 第一次初始化时如果是没有网络，则不会自动重连，其他情况初始化下会自动重连
         */
        if (!mLinkKitInit) {
            LinkKit.getInstance().init(mContext, params, new ILinkKitConnectListener() {
                @Override
                public void onError(AError aError) { // 初始化失败 error包含初始化错误信息
                    mLinkKitInit = false;
                    AdaLog.d(Key.LOG, Tag, "LinkKit.init.onError", "AError:" + AliSdkUtils.aErrToString(aError));
                }

                @Override
                public void onInitDone(Object data) {// 初始化成功 data 作为预留参数
                    mLinkKitInit = true;
                    AdaLog.d(Key.LOG, Tag, "LinkKit.init.onInitDone", "Object:" + AliSdkUtils.objToString(data));
                    if (mVerifySdkInitCode == 0) { //本地授权通过，不需要网络申请授权
                    } else if (mVerifySdkInitCode == 5010) { //时间问题未通过,这里也不需要网络申请授权
                    } else {
                        uploadNeedAuthEvent();
                    }
                }
            });
        }
        /**
         * 监听下发的数据
         */
        if (!mRegPushListener) {
            mRegPushListener = true;
            LinkKit.getInstance().registerOnPushListener(mOnPushListener);
        }


    }


    private IConnectNotifyListener mOnPushListener = new IConnectNotifyListener() {
        // 云端下行数据回调
        // connectId 连接类型 topic 下行 topic; aMessage 下行数据
        // String pushData = new String((byte[]) aMessage.data);
        // pushData 示例  {"method":"thing.service.test_service","id":"123374967","params":{"vv":60},"version":"1.0.0"}
        // method 服务类型； params 下推数据内
        @Override
        public void onNotify(final String connectId, final String topic, final AMessage aMessage) {
            AdaLog.d(Key.LOG, Tag, "LinkKit.registerOnPushListener.onNotify", "connectId:" + connectId + " topic:" + topic + " aMessage:" + AliSdkUtils.aMsgToString(aMessage));
            FaceDoorThreadPool.getInstance().getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    mRegPushListener = true;
                    MqttPublishRequest request = mEventHandler.handleEvent(connectId, topic, aMessage);
                    publishResponseMsg(request);
                }
            });

        }

        // 选择是否不处理某个 topic 的下行数据
        // 如果不处理某个topic，则onNotify不会收到对应topic的下行数据
        @Override
        public boolean shouldHandle(String connectId, String topic) {
            AdaLog.d(Key.LOG, Tag, "LinkKit.registerOnPushListener.shouldHandle", "connectId:" + connectId + " topic:" + topic);
            return true;
        }

        @Override
        public void onConnectStateChange(String connectId, ConnectState connectState) {
            AdaLog.d(Key.LOG, Tag, "LinkKit.registerOnPushListener.onConnectStateChange", "connectState:" + connectState);
            mConnectState = connectState;
            mOnAliSDKListener.onIotConnectState(connectState);
        }
    };


    private void publishResponseMsg(MqttPublishRequest request) {
        if (request == null) {
            return;
        }
        LinkKit.getInstance().publish(request, new IConnectSendListener() {
            @Override
            public void onResponse(ARequest aRequest, AResponse aResponse) {
                AdaLog.d(Key.LOG, Tag, "LinkKit.publish.onResponse", "aRequest:" + AliSdkUtils.aRequestToString(aRequest) + " aResponse:" + aResponse.data);
            }

            @Override
            public void onFailure(ARequest aRequest, AError aError) {
                AdaLog.d(Key.LOG, Tag, "LinkKit.publish.onFailure", "aRequest:" + aRequest.toString() + " aResponse:" + AliSdkUtils.aErrToString(aError));
            }
        });
    }


    public void uploadNeedAuthEvent() {
        AdaLog.d(Key.LOG, Tag, "LinkKit.thingEventPost.uploadNeedAuthEvent", "");
        // 获取VerifySDK授权信息

        VerifySDKManager.getInstance().setMinFaceDetectSize(0.1f);
        // 上报设备需授权事件
        HashMap<String, ValueWrapper> hashMap = new HashMap<>(16);
        hashMap.put("Token", new ValueWrapper.StringValueWrapper(mToken));
        hashMap.put("DeviceEncrypt", new ValueWrapper.StringValueWrapper(mEncrypt));
        OutputParams params = new OutputParams(hashMap);
        LinkKit.getInstance().getDeviceThing().thingEventPost(EVENT_NEED_AUTH_VERIFY, params, new IPublishResourceListener() {
            @Override
            public void onSuccess(String resId, Object object) { //事件上报成功
                AdaLog.d(Key.LOG, Tag, "LinkKit.thingEventPost.uploadNeedAuthEvent", "resId:" + resId + " object:" + object);
            }

            @Override
            public void onError(String resId, AError aError) { //事件上报失败
                AdaLog.d(Key.LOG, Tag, "LinkKit.thingEventPost.uploadNeedAuthEvent", "resId:" + resId + " aError:" + AliSdkUtils.aErrToString(aError));
            }
        });
    }


    /**
     * 上传属性文件
     */
    public void uploadPropertyFile() {
        String propertyStr = PropertyUtils.readProperty();
        Map<String, ValueWrapper> propertyMap = PropertyUtils.getPropertyMap(propertyStr);
        LinkKit.getInstance().getDeviceThing().thingPropertyPost(propertyMap, new IPublishResourceListener() {
            @Override
            public void onSuccess(String resID, Object object) {
                AdaLog.d(Key.LOG, Tag, "uploadPropertyFile", "onSuccess" + "resID:" + resID);
            }

            @Override
            public void onError(String resId, AError aError) {
                AdaLog.d(Key.LOG, Tag, "uploadPropertyFile", "onError" + "resID:" + resId);
            }
        });
    }


    public void loadLib() {
        final boolean isLibLoaded = VerifySDKManager.getInstance().isUserLibLoaded();
        if (!isLibLoaded) {
            AdaLog.d(Key.LOG, Tag, "loadLib", " === start ===" + " isUserLibLoaded:" + isLibLoaded);
            mOnAliSDKListener.onLocalFaceLoading(true);
            VerifySDKManager.getInstance().loadUserLib(new VerifySdkListener() {
                @Override
                public void onBatchUserLibUpdate(int errorCode) {
                    AdaLog.d(Key.LOG, Tag, "loadLib.onBatchUserLibUpdate", "=== end === " + "  errorCode:" + errorCode);
                }

                //用户库加载结果回调
                @Override
                public void onUserLibLoaded(int errorCode) {
                    mOnAliSDKListener.onLocalFaceLoading(false);
                    int quantity = VerifySDKManager.getInstance().getUserQuantity();
                    AdaLog.d(Key.LOG, Tag, "loadLib.onUserLibLoaded", "=== end === " + "  errorCode:" + errorCode + " getUserQuantity:" + quantity);
                    EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_QUANTITY, String.valueOf(quantity)));
                }

                //用户库清空结果回调
                @Override
                public void onUserLibEmpty(int errorCode) {
                    AdaLog.d(Key.LOG, Tag, "loadLib.onUserLibEmpty", "=== end === " + "  errorCode:" + errorCode);
                }
            });
        }
    }


    /**
     * 发送文件
     *
     * @param filePath
     */
    public void sendFileThroughH2(String filePath) {
        AdaLog.d(Key.LOG, Tag, "sendFileThroughH2", "filePath:" + filePath);
        mBaseH2Util.sendFile(filePath, new CompletableListener<Http2Response>() {
            @Override
            public void complete(Http2Response http2Response) {
                AdaLog.d(Key.LOG, Tag, "sendFileThroughH2.sendFile.complete", "headers:" + http2Response.getHeaders() + " file name:" + http2Response.getHeaders().get("x-file-path").toString());
                FaceDoorProperty faceDoorProperty = PropertyUtils.getProperty();
                if (null == faceDoorProperty) {
                    AdaLog.d(Key.LOG, Tag, "sendFileThroughH2.sendFile.complete", "property file error");
                    mBaseH2Util.closeStream();
                    mBaseH2Util.disconnect(null);
                    return;
                }
                faceDoorProperty.setAddedFaceSetName(http2Response.getHeaders().get("x-file-path").toString());
                PropertyUtils.writeProperty(faceDoorProperty);
                mBaseH2Util.closeStream();
                mBaseH2Util.disconnect(null);
            }

            @Override
            public void completeExceptionally(Throwable throwable) {
                AdaLog.d(Key.LOG, Tag, "sendFileThroughH2.sendFile.completeExceptionally", "throwable:" + throwable);
                mBaseH2Util.closeStream();
                mBaseH2Util.disconnect(null);
            }
        });
    }


    /**
     * 清空所有人脸库数据
     */
    public void clearAllFaceData() {
        if (VerifySDKManager.getInstance().isUserLibLoaded()) {
            VerifySDKManager.getInstance().clearUserLib(true, new VerifySdkListener() {
                @Override
                public void onBatchUserLibUpdate(int errorCode) {

                }

                //用户库加载结果回调
                @Override
                public void onUserLibLoaded(int errorCode) {

                }

                //用户库清空结果回调
                @Override
                public void onUserLibEmpty(int errorCode) {

                    AdaLog.d(Key.LOG, Tag, "clearAllFaceData.onUserLibEmpty", "errorCode：" + errorCode);
                    if (errorCode == 0) {
                        boolean del1 = FileUtils.deleteFile("/sdcard/AddedUser");
                        AdaLog.d(Key.LOG, Tag, "clearAllFaceData.onUserLibEmpty", "del /sdcard/AddedUser :" + del1);
                        EventBus.getDefault().post(new MsgEvent("clearAllFaceData().del /sdcard/AddedUser :" + del1));

                        boolean del2 = FileUtils.deleteFile("/sdcard/CurrentFaces.json");
                        AdaLog.d(Key.LOG, Tag, "clearAllFaceData.onUserLibEmpty", "del /sdcard/CurrentFaces.json :" + del2);
                        EventBus.getDefault().post(new MsgEvent("clearAllFaceData().del /sdcard/CurrentFaces.json :" + del2));

                        boolean del3 = FileUtils.deleteFile("/sdcard/property");
                        AdaLog.d(Key.LOG, Tag, "clearAllFaceData.onUserLibEmpty", "del /sdcard/property :" + del3);
                        EventBus.getDefault().post(new MsgEvent("clearAllFaceData().del /sdcard/property :" + del3));

                        boolean del4 = FileUtils.deleteFile("/storage/emulated/0/adacampus/main/dev/ali/face/saved_faces.json");
                        AdaLog.d(Key.LOG, Tag, "clearAllFaceData.onUserLibEmpty", "del /storage/emulated/0/adacampus/main/dev/ali/face/saved_faces.json :" + del4);
                        EventBus.getDefault().post(new MsgEvent("clearAllFaceData().del /storage/emulated/0/adacampus/main/dev/ali/face/saved_faces.json :" + del4));

                        SharedPreferencesUtil.saveFaceSyncProgress(-1.0f);
                        AdaLog.d(Key.LOG, Tag, "clearAllFaceData.onUserLibEmpty", "saveFaceSyncProgress : " + -1.0f);
                        EventBus.getDefault().post(new MsgEvent("clearAllFaceData().saveFaceSyncProgress : " + -1.0f));

                        BaseApp.showLongToast("已重置人脸数据，正在重启...");
                        AdaLog.d(Key.LOG, Tag, "clearAllFaceData.onUserLibEmpty", "已重置人脸数据，正在重启...");
                        EventBus.getDefault().post(new MsgEvent("clearAllFaceData().已重置人脸数据，正在重启..."));
                        MainApp.postUIThreadDelay(new Runnable() {
                            @Override
                            public void run() {
                                AppUtils.stopApp(BaseApp.getApp().getMainAppId());
                            }
                        }, 2000);
                    }
                }
            });
        }
    }

    /**
     * 获取当前人脸库人脸数量
     */
    public int getVerifySdkUserQuantity() {
        if (VerifySDKManager.getInstance().isUserLibLoaded()) {
            return VerifySDKManager.getInstance().getUserQuantity();
        }
        return -1;
    }


    /**
     * 保存人脸识别帧图片到本地
     *
     * @param faceData
     * @param height
     * @param width
     * @param filePath
     */
    private void saveImage(byte[] faceData, int height, int width, String filePath) {
        YuvImage image = new YuvImage(faceData, ImageFormat.NV21, width, height, null);
        ByteArrayOutputStream outputSteam = new ByteArrayOutputStream();
        //  将NV21格式图片，以质量70压缩成Jpeg，并得到JPEG数据流
        image.compressToJpeg(new Rect(0, 0, image.getWidth(), image.getHeight()), 100, outputSteam);
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(outputSteam.toByteArray(), 0, outputSteam.toByteArray().length);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
