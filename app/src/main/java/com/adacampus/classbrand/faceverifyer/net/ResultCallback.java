package com.adacampus.classbrand.faceverifyer.net;

import android.text.TextUtils;

import com.adacampus.classbrand.library.net.api.ErrorResponse;
import com.adacampus.classbrand.library.utils.AdaLog;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class ResultCallback<T> implements Callback<T> {

    private static final Charset UTF8 = Charset.forName("UTF-8");

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        String errorBody = "";
        String body = "";

        try {
            if (response.errorBody() != null) {
                errorBody = response.errorBody().string();
            }
            if (response.body() != null) {
                body = response.body().toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String log = String.format("url=%s\ncode=%s\nmessage=%s\nerrorBody=%s\nbody=%s",
                call.request().url().toString(),
                response.code(),
                response.message(),
                errorBody,
                body);

        AdaLog.d(AdaLog.MODULE_DEVICE, com.adacampus.classbrand.library.net.api.ResultCallback.class, "onResponse", log);
        if (response.isSuccessful()) {
            onResponseSuccessful(response.body());
            onFinish(call);
        } else {
            ErrorResponse errorResponse = new ErrorResponse();
            ResponseBody responseBody = response.errorBody();
            String errorBodyStr = null;
            if (responseBody != null) {
                errorBodyStr = response.toString();
            }
            if (TextUtils.isEmpty(errorBodyStr)) {
                errorResponse = new Gson().fromJson(errorBodyStr, ErrorResponse.class);
                onResponseFailure(errorResponse);
                onFinish(call);
                return;
            }

            String errorMsg = createErrorMsg(response);
            if (response.code() == 500) { //500没有错误格式,需要自定义
                onResponseFailure(ErrorResponse.create500Error(errorMsg));
                onFinish(call);
                return;
            }
            if (response.code() == 502) { //502没有错误格式，需要自定义
                onResponseFailure(ErrorResponse.create502Error(errorMsg));
                onFinish(call);
                return;
            }

            try {
                if (!TextUtils.isEmpty(errorMsg)) {
                    errorResponse = new Gson().fromJson(errorMsg, ErrorResponse.class);
                    onResponseFailure(errorResponse);
                    onFinish(call);
                }
            } catch (Exception e) {
                e.printStackTrace();
                errorResponse.error_message = e.getMessage();
                onResponseFailure(errorResponse);
                onFinish(call);
            }
        }
    }

    public abstract void onResponseSuccessful(T result);

    public abstract void onResponseFailure(ErrorResponse errorResponse);

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        AdaLog.e(AdaLog.MODULE_DEVICE, com.adacampus.classbrand.library.net.api.ResultCallback.class, "onResponse", call.request().url().toString() + "\n" + "error:" + t.getMessage());
        if (t instanceof UnknownHostException
                || t instanceof SocketException) {
            onNetworkDisconnected();
        }
        onFinish(call);
    }

    public void onFinish(Call<T> call) {

    }

    //无网络
    public void onNetworkDisconnected() {

    }


    /**
     * 创建补充http请求返回错误的错误提示信息
     * 暂用于404
     *
     * @param response
     * @return
     */
    private String createErrorMsg(Response<T> response) {
        if (response.errorBody() == null) {
            return "";
        }
        ResponseBody errorBody = response.errorBody();

        if (errorBody == null) {
            return "";
        }

//        long contentLength = errorBody.contentLength();
//        if (contentLength <= -1) { //有内容时有可能出现-1的问题
//            return "";
//        }
        try {
            BufferedSource source = errorBody.source();
            source.request(Long.MAX_VALUE);
            Buffer buffer = source.buffer();
            Charset charset = UTF8;
            MediaType contentType = errorBody.contentType();
            if (contentType != null) {
                charset = contentType.charset(UTF8);
                return buffer.clone().readString(charset);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
