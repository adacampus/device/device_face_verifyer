package com.adacampus.classbrand.faceverifyer.face;


import android.content.Context;
import android.text.TextUtils;

import com.adacampus.classbrand.faceverifyer.alisdk.OnAliSDKListener;
import com.adacampus.classbrand.faceverifyer.alisdk.VerifySdkListener;
import com.adacampus.classbrand.faceverifyer.face.report.FaceCountReportLoader;
import com.adacampus.classbrand.faceverifyer.utils.CommonEvent;
import com.adacampus.classbrand.library.net.api.EmptyResult;
import com.adacampus.classbrand.library.net.mq.EventHandler;
import com.adacampus.classbrand.library.net.mq.IEventHandler;
import com.adacampus.classbrand.library.utils.AdaLog;
import com.adacampus.classbrand.library.utils.FilePath;
import com.adacampus.classbrand.library.utils.FileUtils;
import com.adacampus.classbrand.faceverifyer.net.BaseDataController;
import com.adacampus.classbrand.faceverifyer.net.ListDataParser;
import com.adacampus.classbrand.faceverifyer.utils.MsgEvent;
import com.adacampus.classbrand.faceverifyer.utils.Key;
import com.adacampus.classbrand.faceverifyer.utils.SharedPreferencesUtil;
import com.alibaba.security.rp.verifysdk.service.VerifySDKManager;
import com.alibaba.security.rp.verifysdk.util.Type;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lzy.okgo.model.Progress;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Envelope;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FaceDataManager implements IEventHandler, BaseDataController.UpdateCallback, FaceImageDownloader.OnFaceImageDownloadCallback {

    private FaceListDataController mFaceListDataController;
    private FaceImageDownloader mDownloader;
    private HashMap<String, Face> mSavedFacesMap;
    private OnAliSDKListener mOnAliSDKListener;

    private int mAllSyncCount = 0;
    private int mAllAddCount = 0;
    private int mAllDelCount = 0;
    private int mAddSuccess = 0;
    private int mDelSuccess = 0;
    private int mAddFailure = 0;
    private int mDelFailure = 0;
    private int mUpdateCount = 0;
    private FaceDataSyncEventHandler mEvntHandler;
    private final String mFileName = "saved_faces.json";

    private class FaceDataSyncEventHandler extends EventHandler {

        public FaceDataSyncEventHandler() {
//            mEventKey = Key.KEY_EVENT_FACE_SET_UPDATE;
        }


        @Override
        public void handleEvent(JSONObject json, Channel channel, Envelope envelope) {
//            EventBus.getDefault().post(new MsgEvent(Key.KEY_EVENT_FACE_SET_UPDATE));
            mAllSyncCount = 0;
            mAllAddCount = 0;
            mAddSuccess = 0;
            mAddFailure = 0;
            mAllDelCount = 0;
            mDelSuccess = 0;
            mDelFailure = 0;
            mUpdateCount = 0;
            EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_ADD_CHANGE, mAllAddCount + "/" + mAddSuccess + "/" + mAddFailure));
            EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_DEL_CHANGE, mAllDelCount + "/" + mDelSuccess + "/" + mDelFailure));
            EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_UPDATE_COUNT, String.valueOf(mUpdateCount)));
            loadData();
            ack();
        }
    }

    @Override
    public EventHandler getEventHandler() {
        return mEvntHandler;
    }

    public FaceDataManager(Context context, OnAliSDKListener onAliSDKListener) {
        FaceListDataLoader<List<Face>> loader = new FaceListDataLoader<>();
        ListDataParser<Face> parser = new ListDataParser<>();
        mFaceListDataController = new FaceListDataController(parser, loader);
        mFaceListDataController.setUpdateCallback(this);
        mOnAliSDKListener = onAliSDKListener;
        mDownloader = new FaceImageDownloader(context);
        mDownloader.setFaceImageDownloadCallback(this);
        mSavedFacesMap = readFaceData();
        mEvntHandler = new FaceDataSyncEventHandler();
        float proress = SharedPreferencesUtil.getFaceSyncProgress();
        if (proress > 0.0f) {
            mOnAliSDKListener.onSyncFaceProgress(proress);
        } else {
            mOnAliSDKListener.onSyncFaceProgress(0.0f);
        }

    }

    public void loadData() {
        mFaceListDataController.mDataLoader.cancel();
        mFaceListDataController.mDataLoader.reset();
        mFaceListDataController.mDataLoader.loadData();
    }


    private HashMap<String, Face> readFaceData() {
        File file = new File(FilePath.getFaceDataPath() + File.separator + mFileName);
        String faceJson = FileUtils.readTextFile(file);
        AdaLog.d(Key.LOG, FaceDataManager.this, "readFaceData", "json:" + faceJson);
        if (faceJson != null) {
            faceJson = faceJson.trim();
        }
        if (TextUtils.isEmpty(faceJson)) {
            return new HashMap<>();
        }
        if (!TextUtils.isEmpty(faceJson) && (!faceJson.startsWith("{") || !faceJson.endsWith("}"))) {
            AdaLog.d(Key.LOG, FaceDataManager.this, "readFaceData", "本地json文件格式不正确:" + faceJson);
            return new HashMap<>();
        }
        Gson gson = new Gson();
        java.lang.reflect.Type type = new TypeToken<HashMap<String, Face>>() {
        }.getType();
        HashMap<String, Face> map = gson.fromJson(faceJson, type);
        return map;
    }

    private void saveFaceData(HashMap<String, Face> map) {
        Gson gson = new Gson();
        String json = gson.toJson(map).trim();
        File file = new File(FilePath.getFaceDataPath() + File.separator + mFileName);
        FileUtils.saveTextFile(json.trim(), file);
        AdaLog.d(Key.LOG, FaceDataManager.this, "saveFaceData", "json:" + json);
    }


    public void reportFaceCount() {
        FaceCountReportLoader<EmptyResult> loader = new FaceCountReportLoader<>();
        int quantity = VerifySDKManager.getInstance().getUserQuantity();
        loader.setCount(quantity);
        loader.loadData();
    }

    @Override
    public void onRefresh() {
        mAllSyncCount = mFaceListDataController.mListDataParser.mList.size();
        mAllAddCount = 0;
        mAddSuccess = 0;
        mAddFailure = 0;
        mAllDelCount = 0;
        mDelSuccess = 0;
        mDelFailure = 0;
        mUpdateCount = 0;
        EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_SYNC_COUNT, mAllSyncCount + ""));
        EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_ADD_CHANGE, mAllAddCount + "/" + mAddSuccess + "/" + mAddFailure));
        EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_DEL_CHANGE, mAllDelCount + "/" + mDelSuccess + "/" + mDelFailure));
        EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_UPDATE_COUNT, String.valueOf(mUpdateCount)));
        delFaces();
        logAddFaceCount();
        addFaces();
        reportFaceCount();
    }

    private void delFaces() {
        HashMap<String, Face> newMap = new HashMap<>();
        //格式转换
        for (Face face : mFaceListDataController.mListDataParser.getData()) {
            newMap.put(face.face_ident, face);
        }

        List<Face> needDelFace = new ArrayList<>();
        for (Map.Entry<String, Face> entry : mSavedFacesMap.entrySet()) {
            if (!newMap.containsKey(entry.getKey())) {
                needDelFace.add(entry.getValue());
            }
        }
        mAllDelCount = needDelFace.size();
        EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_DEL_CHANGE, mAllDelCount + "/" + mDelSuccess + "/" + mDelFailure));
        for (Face face : needDelFace) {
            VerifySDKManager.getInstance().removeUser(true, face.getFixFaceIdent(), new DelFaceListener(face));
        }
    }

    private void logAddFaceCount() {
        List<Face> tempAllFace = mFaceListDataController.mListDataParser.getData();
        for (Face face : tempAllFace) {
            Face savedFace = mSavedFacesMap.get(face.face_ident);
            if (savedFace == null) {
                mAllAddCount++;
                continue;
            }
            if (!(savedFace.face_hash.equals(face.face_hash))) { //hash被改变，图片被修改过需要重新同步
                mAllAddCount++;
                mUpdateCount++;
            }
        }
        EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_ADD_CHANGE, mAllAddCount + "/" + mAddSuccess + "/" + mAddFailure));
        EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_UPDATE_COUNT, String.valueOf(mUpdateCount)));

    }

    private void addFaces() {
        downLoadNext();
    }

    private void downLoadNext() {
        if (mFaceListDataController.mListDataParser.hasNext()) {
            Face face = mFaceListDataController.mListDataParser.getNext();
            Face savedFace = mSavedFacesMap.get(face.face_ident);
            if (savedFace == null) {
                mDownloader.download(face);
                return;
            }
            if (!(savedFace.face_hash.equals(face.face_hash))) { //hash被改变，图片被修改过需要重新同步
                mDownloader.download(face);
            } else {
                downLoadNext();
            }
        }
    }

    @Override
    public void onLoadFaceImageFailure(Face face, Progress progress) {
        downLoadNext();
    }

    @Override
    public void onLoadFaceImageSuccessful(Face face, File file) {
        final byte[] imgBytes = readImageFile(file);
        byte[] zipData = zip(imgBytes);
        VerifySDKManager.getInstance().addUser(true, face.getFixFaceIdent(), Type.BIOLOGY_FACE, imgBytes, new AddFaceListener(face));
        downLoadNext();
    }


    private class DelFaceListener extends VerifySdkListener {

        private Face mFace;

        public DelFaceListener(Face face) {
            mFace = face;
        }

        @Override
        public void onSingleUserLibUpdate(String face_ident, int errorCode) {
            EventBus.getDefault().post(new MsgEvent("删除人脸:" + Face.getOriginFaceIdent(face_ident) + "  errorCode:" + errorCode));
            AdaLog.d(Key.LOG, DelFaceListener.this, "onSingleUserLibUpdate", "face_ident:" + face_ident + " errorCode:" + errorCode);
            if (errorCode == 0) {
                mDelSuccess++;
                if (mFace.getFixFaceIdent().equals(face_ident)) {
                    mSavedFacesMap.remove(mFace.face_ident);
                }
            } else {
                mDelFailure++;
            }
            EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_DEL_CHANGE, mAllDelCount + "/" + mDelSuccess + "/" + mDelFailure));
            sendUserQuantity();
        }
    }


    private class AddFaceListener extends VerifySdkListener {
        private Face mFace;

        public AddFaceListener(Face face) {
            mFace = face;
        }

        @Override
        public void onSingleUserLibUpdate(String face_ident, int errorCode) {
            AdaLog.d(Key.LOG, AddFaceListener.this, "onSingleUserLibUpdate", "face_ident:" + face_ident + " errorCode:" + errorCode);
            EventBus.getDefault().post(new MsgEvent("添加人脸:" + Face.getOriginFaceIdent(face_ident) + "  errorCode:" + errorCode));
            if (errorCode == 0) {
                if (mFace.getFixFaceIdent().equals(face_ident)) {
                    mAddSuccess++;
                    if (mOnAliSDKListener != null) {
                        updateProgress();
                    }
                    mSavedFacesMap.put(mFace.face_ident, mFace);
                    saveFaceData(mSavedFacesMap);
                }
            } else {
                mAddFailure++;
            }
            EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_ADD_CHANGE, mAllAddCount + "/" + mAddSuccess + "/" + mAddFailure));
            sendUserQuantity();
        }
    }

    private void sendUserQuantity() {
        int quantity = VerifySDKManager.getInstance().getUserQuantity();
        EventBus.getDefault().post(new CommonEvent(CommonEvent.TYPE_QUANTITY, String.valueOf(quantity)));
    }

    public void updateProgress() {
        if (mFaceListDataController.mListDataParser.isEmpty()) {
            return;
        }
        int size = mFaceListDataController.mListDataParser.getData().size();
        float precent = ((float) mAddSuccess / size) * 100.0f;

        AdaLog.d(Key.LOG, FaceDataManager.this, "updateProgress",
                "\nmAllSyncCount:" + mAllSyncCount +
                        "\nmAllAddCount:" + mAllAddCount +
                        "\nmAddSuccess:" + mAddSuccess +
                        "\nmAddFailure:" + mAddFailure +
                        "\nmAllDelCount:" + mAllDelCount +
                        "\nmDelSuccess:" + mDelSuccess +
                        "\nmDelFailure:" + mDelFailure +
                        "\nprecent:" + precent);
        mOnAliSDKListener.onSyncFaceProgress(precent);
        SharedPreferencesUtil.saveFaceSyncProgress(precent);
    }


    //压缩图片数据
    public byte[] zip(byte[] data) {
        byte[] b = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ZipOutputStream zip = new ZipOutputStream(bos);
            ZipEntry entry = new ZipEntry("~~~1.bmp");
            entry.setSize(data.length);//返回条目数据的未压缩大小；如果未知，则返回 -1。
            zip.putNextEntry(entry);// 开始写入新的 ZIP 文件条目并将流定位到条目数据的开始处
            zip.write(data);//将字节数组写入当前 ZIP 条目数据。
            zip.closeEntry();
            zip.close();
            b = bos.toByteArray();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return b;
    }

    private byte[] readImageFile(File file) {
        try {
            InputStream is = new FileInputStream(file);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024 * 10];
            int n = 0;
            while ((n = is.read(buffer)) != -1) {
                out.write(buffer, 0, n);
            }
            return out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }
}
