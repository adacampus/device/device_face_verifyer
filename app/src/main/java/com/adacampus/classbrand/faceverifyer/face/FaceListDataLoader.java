package com.adacampus.classbrand.faceverifyer.face;

import com.adacampus.classbrand.library.net.api.ErrorResponse;
import com.adacampus.classbrand.library.net.api.ResultCallback;
import com.adacampus.classbrand.faceverifyer.net.CommonApiServer;
import com.adacampus.classbrand.faceverifyer.net.DataLoader;
import com.adacampus.classbrand.faceverifyer.net.SysRetrofitHelper;

import java.util.List;

import retrofit2.Call;

public class FaceListDataLoader<T> extends DataLoader<T> {

    private CommonApiServer mApiServer;
    public String mCardNumber;
    public Call<List<Face>> mCall;

    public FaceListDataLoader() {
        mApiServer = SysRetrofitHelper.getInstance().getRetrofit().create(CommonApiServer.class);

    }

    @Override
    public void loadData() {
        mCall.enqueue(new ResultCallback<List<Face>>() {
            @Override
            public void onResponseSuccessful(List<Face> data) {
                mData = (T) data;
                if (mDataLoaderCallBack != null) {
                    mDataLoaderCallBack.onDataUpdate(mData);
                }
            }

            @Override
            public void onResponseFailure(ErrorResponse errorResponse) {
                if (mDataLoadFailureCallBack != null) {
                    mDataLoadFailureCallBack.onDataLoadFailed();
                }
            }

            @Override
            public void onFailure(Call<List<Face>> call, Throwable t) {
                super.onFailure(call, t);
                if (mDataLoadFailureCallBack != null) {
                    mDataLoadFailureCallBack.onDataLoadFailed();
                }
            }

        });
    }

    public void setCardNumber(String cardNumber) {
        mCardNumber = cardNumber;
    }

    @Override
    public void cancel() {
        if (mCall != null) {
            if (!mCall.isCanceled()) {
                mCall.cancel();
            }
        }
    }

    @Override
    public void reset() {
        mCall = mApiServer.getFaceList();
    }

}
