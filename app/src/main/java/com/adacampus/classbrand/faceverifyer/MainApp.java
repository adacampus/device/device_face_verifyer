package com.adacampus.classbrand.faceverifyer;

import android.content.Context;

import androidx.multidex.MultiDex;

import com.adacampus.classbrand.library.base.BaseApp;
import com.adacampus.classbrand.faceverifyer.utils.AppUncaughtExceptionHandler;
import com.adacampus.classbrand.library.utils.AppUtils;


public class MainApp extends BaseApp {

    public static final String LOG_TAG = "ADA-FACE";


    @Override
    public String getBuildType() {
        return BuildConfig.BUILD_TYPE;
    }

    @Override
    public String getLogTag() {
        return LOG_TAG;
    }

    @Override
    public String getWatcherAppId() {
        return null;
    }

    @Override
    public String getMainAppId() {
        return BuildConfig.APPLICATION_ID;
    }

    @Override
    public String getPatchAppId() {
        return null;
    }

    @Override
    public String getAppType() {
        return AppUtils.APP_TYPE_FACE_VERIFYER_APP;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setAppExceptionHandler();
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    private void setAppExceptionHandler() {
        Thread.setDefaultUncaughtExceptionHandler(new AppUncaughtExceptionHandler());
    }


}

