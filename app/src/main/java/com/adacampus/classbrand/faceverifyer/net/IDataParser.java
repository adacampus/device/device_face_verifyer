package com.adacampus.classbrand.faceverifyer.net;

public interface IDataParser<T> {

    public T getNext();

    public T getPrevious();

    public T getCurrent();

    public boolean isEmpty();

    public boolean hasNext();


}
