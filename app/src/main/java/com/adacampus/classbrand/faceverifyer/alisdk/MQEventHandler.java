package com.adacampus.classbrand.faceverifyer.alisdk;

import android.content.Context;
import android.text.TextUtils;

import com.adacampus.classbrand.library.utils.AdaLog;
import com.adacampus.classbrand.faceverifyer.utils.Key;
import com.aliyun.alink.linksdk.cmp.connect.channel.MqttPublishRequest;
import com.aliyun.alink.linksdk.cmp.core.base.AMessage;

import java.util.List;

public class MQEventHandler {

    public static final String ID_CONNECTED = "LINK_PERSISTENT";
    public static final String EVENT_SET_PROPERTY = "set";
    public static final String EVENT_AUTH_VERIFY_SERVICE = "AuthVerifySDK";
    public static final String EVENT_SYNC_FACE_PIC_SERVICE = "SyncFacePictures";
    public static final String EVENT_QUERY_ADDEDUSER_SERVICE = "QueryAddedUserInfo";
    public static final String EVENT_QUERY_SYNC_PIC_SCH_SERVICE = "QuerySyncPicSchedule";

    private String mRRPCTopic;

    private Context mContext;


    public MQEventHandler(Context context, String rrpcTopic) {
        mContext = context;
        mRRPCTopic = rrpcTopic;
        PropertyUtils.checkProperty();
    }



    //处理下发的事件
    public MqttPublishRequest handleEvent(String connectId, String topic, final AMessage aMessage) {
        if (aMessage == null) {
            return null;
        }
        String serviceIdentifier = AliSdkUtils.parseIdentifierFromMsg(aMessage);
        switch (serviceIdentifier) {
            case EVENT_SET_PROPERTY:  // 属性设置事件
                handlePropertyMsg(aMessage);
                break;
            case EVENT_AUTH_VERIFY_SERVICE: //授权下发事件
                handleAuthVerify(aMessage);
                break;
//            case EVENT_SYNC_FACE_PIC_SERVICE: //人脸库同步事件
//                FaceDoorThreadPool.getInstance().getThreadPool().execute(new Runnable() {
//                    @Override
//                    public void run() {
//                        String facePicturesUrl = AliSdkUtils.parseParamValueFromMsg(aMessage, "FacePicURL");
//                        handleSyncPic(facePicturesUrl);
//                    }
//                });
//                break;
            case EVENT_QUERY_ADDEDUSER_SERVICE: //查询当前的的人脸图，无需操作后返回结果
                break;
            case EVENT_QUERY_SYNC_PIC_SCH_SERVICE:
                break;
            default: //其他未添加事件
                break;
        }

        //返回组装好的request请求消息
        return assembleResponseRequest(serviceIdentifier, connectId, topic, aMessage);
    }


    //处理"照片同步"事件
//    private void handleSyncPic(String facePicturesUrl) {
//        AdaLog.d(AdaLog.MODULE_ALI_SDK, AliSdkManager.Tag, "handleSyncPic", "facePicturesUrl:" + facePicturesUrl);
//        setSyncRate(0.0);
//        mFaceSyncHelper.downloadSyncFacePictures(facePicturesUrl);
//    }

    //处理"授权"事件
    private void handleAuthVerify(final AMessage aMessage) {
        AdaLog.d(Key.LOG, AliSdkManager.Tag, "handleAuthVerify", "aMessage:" + AliSdkUtils.aMsgToString(aMessage));
        String license = AliSdkUtils.parseParamValueFromMsg(aMessage, "LicenseData");
        if (TextUtils.isEmpty(license)) {
            AdaLog.d(Key.LOG, AliSdkManager.Tag, "handleAuthVerify", "license: empty");
            AliSdkManager.getInstance().reInitVerifySdkError();
        } else {
            AdaLog.d(Key.LOG, AliSdkManager.Tag, "handleAuthVerify", "license: " + license);
            AliSdkManager.getInstance().setLicense(license);
            AliSdkManager.getInstance().retryInitVerifySdk();
        }

    }


    //处理"属性"事件
    private void handlePropertyMsg(final AMessage aMessage) {
        AdaLog.d(Key.LOG, AliSdkManager.Tag, "handlePropertyMsg", "aMessage:" + AliSdkUtils.aMsgToString(aMessage));
        List<String> propertyKeyList = AliSdkUtils.parseParamNamesFromMsg(aMessage);
        for (int i = 0; i < propertyKeyList.size(); i++) {
            if ("OnMatchThreshOld".equals(propertyKeyList.get(i))) {
                String property = AliSdkUtils.parseParamValueFromMsg(aMessage, "OnMatchThreshOld");
                AliSdkManager.getInstance().setFaceMatchThreshold(Float.parseFloat(property));
                // 更新属性值
                FaceDoorProperty faceDoorProperty = PropertyUtils.getProperty();
                faceDoorProperty.setOnMatchThreshOld(Double.parseDouble(property));
                PropertyUtils.writeProperty(faceDoorProperty);
                PropertyUtils.uploadProperty();
            } else if ("FaceSetSize".equals(propertyKeyList.get(i))) {
                String property = AliSdkUtils.parseParamValueFromMsg(aMessage, "FaceSetSize");
                // 更新属性值
                FaceDoorProperty faceDoorProperty = null;
                faceDoorProperty = PropertyUtils.getProperty();
                faceDoorProperty.setFaceSetSize(Integer.parseInt(property));
                PropertyUtils.writeProperty(faceDoorProperty);
                PropertyUtils.uploadProperty();
            }
        }
    }


    public MqttPublishRequest assembleResponseRequest(String serviceIdentifier, String connectId, String topic, AMessage aMessage) {
        if (TextUtils.isEmpty(serviceIdentifier)) {
            return null;
        }
        if (ID_CONNECTED.equals(connectId) && !topic.isEmpty() && topic.startsWith(mRRPCTopic)) {
            MqttPublishRequest request = new MqttPublishRequest();
            request.isRPC = false;
            request.topic = topic.replace("request", "response");
            String resId = topic.substring(topic.indexOf("rrpc/request/") + 13);

            request.msgId = resId;
            switch (serviceIdentifier) {
                case EVENT_AUTH_VERIFY_SERVICE:
                    answerAuthVerifyService(request);
                    break;
                case EVENT_SYNC_FACE_PIC_SERVICE:
//                    answerSyncFacePicService(request);
                    break;

                case EVENT_QUERY_ADDEDUSER_SERVICE:
//                    answerQueryAddedUserService(request);
                    break;

                case EVENT_QUERY_SYNC_PIC_SCH_SERVICE:
//                    answerQuerySyncPicSchService(request);
                    break;
                default:
                    return null;
            }
            return request;
        }
        return null;
    }


    private void answerAuthVerifyService(MqttPublishRequest request) {
        request.payloadObj = "{\"id\":\"" + request.msgId + "\", \"code\":\"200\",\"data\":{\"DoAuthorized\":0} }";
    }

//    private void answerSyncFacePicService(MqttPublishRequest request) {
//        int syncPicStatus = 0;
//        if (getSyncRate() != SYNC_PIC_OVER_RATE) {
//            syncPicStatus = 1;
//        }
//        request.payloadObj = "{\"id\":\"" + request.msgId + "\", \"code\":\"200\",\"data\":{\"SyncPicStatus\":\"" + syncPicStatus + "\"} }";
//    }

    /**
     * QueryAddedUserInfo消息返回文件类型枚举
     */
    public enum FileType {
        /**
         * 文件存TMP OSS返回的storeid
         */
        StoreID,
        /**
         * 文件存LinkFace返回的文件名
         */
        FileName,
        /**
         * 文件存客户OSS返回的URL地址
         */
        URL;
    }


//    private void answerQueryAddedUserService(MqttPublishRequest request) {
//        //状态：非布控状态
//        int syncStatus = 0;
//        if (mSyncPicRate != SYNC_PIC_OVER_RATE) {
//            //状态：布控中
//            syncStatus = 1;
//        }
//        FileType fileType = FileType.FileName;
//        FaceDoorProperty faceDoorProperty = PropertyUtils.getProperty();
//        if (faceDoorProperty == null) {
//            return;
//        }
//        String filename = faceDoorProperty.getAddedFaceSetName();
//        request.payloadObj = "{\"id\":\"" + request.msgId + "\", \"code\":\"200\",\"data\":" +
//                "{\"StoreID\":\"" + filename + "\", " +
//                "\"Type\":\"" + fileType.ordinal() + "\", " +
//                "\"SyncPicStatus\":\"" + syncStatus + "\"}}";
//    }

//
//    //查询人脸布控进度
//    private void answerQuerySyncPicSchService(MqttPublishRequest request) {
//        double syncRate = 100 * (getSyncRate());
//        request.payloadObj = "{\"id\":\"" + request.msgId + "\", \"code\":\"200\",\"data\":{\"Rate\":\"" + syncRate + "\"}}";
//    }

}
