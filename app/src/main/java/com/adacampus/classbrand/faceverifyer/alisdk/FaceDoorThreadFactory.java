package com.adacampus.classbrand.faceverifyer.alisdk;


import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadFactory;

/**
 * @author panchen
 * @date 10-20-2018
 */
public class FaceDoorThreadFactory implements ThreadFactory {
    private int counter;
    private String name;
    private List<String> stats;

    public FaceDoorThreadFactory(String name) {
        counter = 0;
        this.name = name;
        stats = new ArrayList<String>();
    }

    @Override
    public Thread newThread(@NonNull Runnable runnable) {
        Thread t = new Thread(runnable, name + "-Thread-" + counter);
        counter++;
        stats.add(String.format("Created thread %d with name %s on%s\n" ,t.getId() ,t.getName() ,new Date()));
        return t;
    }
}
