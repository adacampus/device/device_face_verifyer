package com.adacampus.classbrand.faceverifyer.net;

import com.adacampus.classbrand.library.net.mq.EventHandler;
import com.adacampus.classbrand.library.net.mq.IEventHandler;

public abstract class BaseDataController implements IEventHandler {

    public DataLoader mDataLoader;
    protected UpdateCallback mUpdateCallback;
    protected UpdateCallback mExternalUpdateCallback;
    protected CancelCallback mCancelCallback;
    public EventHandler mEventHandler;

    public void setUpdateCallback(UpdateCallback updateCallback) {
        mUpdateCallback = updateCallback;
    }

    /**
     * 用于数据更新时的外部回调 优先级 mUpdateCallback -> mExternalUpdateCallback；
     *
     * @param updateCallback
     */
    public void setExternalUpdateCallback(UpdateCallback updateCallback) {
        mUpdateCallback = updateCallback;
    }

    public interface UpdateCallback {
        public void onRefresh();

    }

    public void setCancelCallback(CancelCallback cancelCallback) {
        mCancelCallback = cancelCallback;
    }

    public interface CancelCallback {
        public void onCancel();
    }


    @Override
    public EventHandler getEventHandler() {
        return mEventHandler;
    }


}
