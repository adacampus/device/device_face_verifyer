package com.adacampus.classbrand.faceverifyer.utils;

public interface Key {
    public static final String LOG = "ALI_FACE";

    public static final String KEY_EVENT_FACE_VERIFICATION = "face.verification";
    public static final String KEY_EVENT_FACE_SET_RESET = "face.set_reset";
}
