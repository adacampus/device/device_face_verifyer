package com.adacampus.classbrand.faceverifyer;

import android.text.Html;
import android.widget.TextView;
import androidx.annotation.NonNull;

import com.taro.headerrecycle.adapter.HeaderRecycleViewHolder;
import com.taro.headerrecycle.adapter.SimpleRecycleAdapter;


public class MsgOption extends SimpleRecycleAdapter.SimpleAdapterOption<String> {

    @Override
    public int getViewType(int position) {
        return 0;
    }

    @Override
    public void setViewHolder(final String msg, int position, @NonNull HeaderRecycleViewHolder holder) {
        final TextView tv = holder.getView(R.id.msg);
        tv.setText(Html.fromHtml(msg));
    }

    @Override
    public int getLayoutId(int viewType) {
        return R.layout.item_msg;
    }
}
