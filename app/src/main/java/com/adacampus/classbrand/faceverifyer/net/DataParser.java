package com.adacampus.classbrand.faceverifyer.net;


public class DataParser<T> implements IDataParser<T> {

    public T mData;
    public int mIndex = 0;

    public void setData(T data) {
        mIndex = 0;
        mData = data;
    }

    public T getData() {
        return mData;
    }

    @Override
    public T getNext() {
        return mData;
    }

    @Override
    public T getPrevious() {
        return mData;
    }

    @Override
    public T getCurrent() {
        return mData;
    }

    @Override
    public boolean isEmpty() {
        if (mData == null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean hasNext() {
        return false;
    }
}
