package com.adacampus.classbrand.faceverifyer.face.verify;

import com.adacampus.classbrand.faceverifyer.face.Face;
import com.adacampus.classbrand.library.net.api.ErrorResponse;
import com.adacampus.classbrand.library.net.api.ResultCallback;
import com.adacampus.classbrand.library.utils.AdaLog;
import com.adacampus.classbrand.faceverifyer.net.CommonApiServer;
import com.adacampus.classbrand.faceverifyer.net.DataLoader;
import com.adacampus.classbrand.faceverifyer.net.SysRetrofitHelper;
import com.adacampus.classbrand.faceverifyer.utils.MsgEvent;
import com.adacampus.classbrand.faceverifyer.utils.Key;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

import retrofit2.Call;

public class SingleFaceLoader<T> extends DataLoader<T> {

    public static final String TAG = "SingleFaceLoader";
    public Call<Face> mCall;
    private String mUserID = "";
    private String mSysBaseUrl = "";

    public SingleFaceLoader() {

    }

    @Override
    public void loadData() {
        mCall.enqueue(new ResultCallback<Face>() {
            @Override
            public void onResponseSuccessful(Face data) {
                AdaLog.d(Key.LOG, TAG, "onResponseSuccessful", data.toString());
                EventBus.getDefault().post(new MsgEvent("获取人脸信息成功:" + data.face_ident));
                mData = (T) data;
                if (mDataLoaderCallBack != null) {
                    mDataLoaderCallBack.onDataUpdate(mData);
                }
            }

            @Override
            public void onResponseFailure(ErrorResponse errorResponse) {
                AdaLog.e(Key.LOG, TAG, "onResponseFailure", "errorResponse:" + errorResponse.toString());
                EventBus.getDefault().post(new MsgEvent("获取人脸信息错误:终止获取:" + errorResponse.error_message));
                if (mDataLoadFailureCallBack != null) {
                    mDataLoadFailureCallBack.onDataLoadFailed();
                }
            }

            @Override
            public void onFailure(Call<Face> call, Throwable t) {
                AdaLog.e(Key.LOG, TAG, "onFailure", "exception:" + t.getMessage());
                if (mDataLoadFailureCallBack != null) {
                    EventBus.getDefault().post(new MsgEvent("获取人脸信息失败:重试获取:" + " \t" + t.getMessage()));
                    mDataLoadFailureCallBack.onDataLoadFailed();
                }
            }

            @Override
            public void onFinish(Call<Face> call) {
                cancel();
                reset();
            }
        });
    }

    @Override
    public void cancel() {
        if (mCall != null) {
            if (!mCall.isCanceled()) {
                mCall.cancel();
            }
        }
    }

    public void setParams(String user_id, String sysBaseUrl) {
        mUserID = user_id;
        mSysBaseUrl = sysBaseUrl;
    }

    @Override
    public void reset() {
        CommonApiServer apiServer = SysRetrofitHelper.getTargetSysRetrofit(mSysBaseUrl).create(CommonApiServer.class);
        mCall = apiServer.getFaceByUserId(mUserID);
    }

}
