package com.adacampus.classbrand.faceverifyer.alisdk;

import com.aliyun.alink.linksdk.cmp.core.base.ConnectState;

/**
 * iotsdk和verify sdk状态回调，用于回调给界面上显示各种元素
 * <p>
 * <p>
 * 回调顺序
 */

public interface OnAliSDKListener {


    /**
     * 授权成功回调，用于显示界面授权图标相关,可以进入首页
     *
     *
     * @param successful 是否成功
     */
    public void onSdkAuth(boolean successful);



    /**
     * 人脸库加载中，本地人脸库加载需要一段时间，用于显示界面本地加载圈
     *
     * @param loading
     */
    public void onLocalFaceLoading(boolean loading);



    /**
     * iot连接状态,暂定用于在界面显示连接，未实现
     */
    public void onIotConnectState(ConnectState connectState);



    /**
     * 人脸库在线同步中，用于显示百分比，
     *
     * @param progress
     */
    public void onSyncFaceProgress(float progress);


}
