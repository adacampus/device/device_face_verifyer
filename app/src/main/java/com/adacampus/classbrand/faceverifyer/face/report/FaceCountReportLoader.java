package com.adacampus.classbrand.faceverifyer.face.report;

import com.adacampus.classbrand.library.net.api.EmptyResult;
import com.adacampus.classbrand.library.net.api.ErrorResponse;
import com.adacampus.classbrand.library.net.api.ResultCallback;
import com.adacampus.classbrand.faceverifyer.net.CommonApiServer;
import com.adacampus.classbrand.faceverifyer.net.DataLoader;
import com.adacampus.classbrand.faceverifyer.net.SysRetrofitHelper;

import retrofit2.Call;

public class FaceCountReportLoader<T> extends DataLoader<T> {

    private CommonApiServer mApiServer;
    public String mCardNumber;
    public Call<EmptyResult> mCall;
    private long mFaceCount = 0;

    public FaceCountReportLoader() {
        mApiServer = SysRetrofitHelper.getInstance().getRetrofit().create(CommonApiServer.class);
        mCall = mApiServer.postFaceCountReport(mFaceCount);

    }

    @Override
    public void loadData() {
        mCall.enqueue(new ResultCallback<EmptyResult>() {
            @Override
            public void onResponseSuccessful(EmptyResult data) {
                mData = (T) data;
                if (mDataLoaderCallBack != null) {
                    mDataLoaderCallBack.onDataUpdate(mData);
                }
            }

            @Override
            public void onResponseFailure(ErrorResponse errorResponse) {
                if (mDataLoadFailureCallBack != null) {
                    mDataLoadFailureCallBack.onDataLoadFailed();
                }
            }

            @Override
            public void onFailure(Call<EmptyResult> call, Throwable t) {
                super.onFailure(call, t);
                if (mDataLoadFailureCallBack != null) {
                    mDataLoadFailureCallBack.onDataLoadFailed();
                }
            }

        });
    }

    @Override
    public void cancel() {
        if (mCall != null) {
            if (!mCall.isCanceled()) {
                mCall.cancel();
            }
        }
    }

    public void setCount(long count) {
        mFaceCount = count;
    }

    @Override
    public void reset() {
        mCall = mApiServer.postFaceCountReport(mFaceCount);
    }

}
