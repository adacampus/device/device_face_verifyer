package com.adacampus.classbrand.faceverifyer.alisdk;

import android.text.TextUtils;
import android.util.Log;

import com.aliyun.alink.h2.api.CompletableListener;
import com.aliyun.alink.h2.api.StreamWriteContext;
import com.aliyun.alink.h2.entity.Http2Request;
import com.aliyun.alink.h2.entity.Http2Response;
import com.aliyun.alink.h2.stream.api.IDownStreamListener;
import com.aliyun.alink.h2.stream.api.IStreamSender;
import com.aliyun.alink.h2.stream.utils.StreamUtil;
import com.aliyun.alink.linkkit.api.LinkKit;
import com.aliyun.alink.linksdk.tools.ALog;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.concurrent.atomic.AtomicBoolean;

import io.netty.handler.codec.http2.Http2Headers;

/**
 * @author XXX
 * @date 10-31-2018
 */
public class BaseH2Util {
    private static final String TAG = "BaseH2Util";

    IStreamSender client = null;
	private String dataStreamId = null;
    Http2Response mHttp2Response = null;
    protected int unlimitedStreamId = 0;
    private AtomicBoolean dataStreamOpened = new AtomicBoolean(false);
    protected final static int TYPE_REQUEST = 0;
    protected final static int TYPE_UNLIMITED_REQUEST = 1;
    protected final static int TYPE_UNLIMITED_REQUEST_END = 2;
    protected final static int TYPE_DOWNSTREAM_REQUEST = 3;
    
    protected void connect(final CompletableListener listener) {

        // 线上版本 保持和设备三元组一致
        client = LinkKit.getInstance().getH2StreamClient();

        client.connect(new CompletableListener<Object>(){

            @Override
            public void complete(Object o) {
                Log.i(TAG, "connect success");
                if (listener != null) {
                    listener.complete(o);
                }
            }

            @Override
            public void completeExceptionally(Throwable throwable) {
                Log.e(TAG, "connect fail");
                if (listener != null) {
                    listener.completeExceptionally(throwable);
                }
            }
        });
    }

    protected void openStream(final String serviceNme, final CompletableListener listener) {
        if (dataStreamOpened.compareAndSet(true, true)) {
            Log.i(TAG, "stream opened.");
            return;
        }
        if (!client.isConnected()){
            Log.e(TAG, "connect first.");
            return;
        }

        ALog.i(TAG, "open stream");
        final Http2Request request = new Http2Request();
        request.getHeaders().add("x-for-test", "ddd");
        client.openStream(serviceNme,
                request, new CompletableListener<Http2Response>() {
                    @Override
                    public void complete(Http2Response http2Response) {
                        ALog.w(TAG, "open stream result: " + http2Response);
                        Log.i(TAG, "open Stream success");
                        if (http2Response != null) {
                            dataStreamId = StreamUtil.getDataStreamId(http2Response.getHeaders());
                            ALog.d(TAG, "dataStreamId=" + dataStreamId);
                            dataStreamOpened.set(true);
                            listener.complete(http2Response);
                        }
                    }

                    @Override
                    public void completeExceptionally(Throwable throwable) {
                        Log.e(TAG,"open Stream failed");
                        Log.w(TAG, "completeExceptionally: ", throwable);
                        dataStreamOpened.set(false);
                        listener.completeExceptionally(throwable);
                    }
                });
    }

    protected void sendStreamData(String data, final int type, int length, final int streamId) {
        ALog.i(TAG, "sendStreamData() called with: data = [" + data + "], type = [" + type + "], streamId = [" + streamId + "]");
        if (client == null || dataStreamOpened.compareAndSet(false, false)) {
            ALog.e(TAG, "open Stream first.");
            return;
        }
        if (TextUtils.isEmpty(data)) {
            return;
        }
        String sendData = data;
        Http2Request request = new Http2Request();
        request.setContent(sendData.getBytes());
        boolean endOfStream = type == TYPE_REQUEST || type == TYPE_DOWNSTREAM_REQUEST;
        request.setEndOfStream(endOfStream);
        request.getHeaders().set("content-length", String.valueOf(length));
        if (type == TYPE_UNLIMITED_REQUEST || type == TYPE_UNLIMITED_REQUEST_END) {
            ALog.d(TAG, "发送无限流");
            request.setEndOfStream(type == TYPE_UNLIMITED_REQUEST_END);
            if (streamId != 0) {
                request.setH2StreamId(streamId);
            }
        } else if (type == TYPE_DOWNSTREAM_REQUEST) {
            ALog.d(TAG, "发送下推流请求");
            request.getHeaders().add("x-test-downstream", "随便填");
        } else if (type == TYPE_REQUEST) {
            ALog.d(TAG, "发送有限流请求");
        } else {
            ALog.e(TAG, "request type error.");
            return;
        }
        client.sendStream(dataStreamId, request,
                new IDownStreamListener() {
                    @Override
                    public void onHeadersRead(String s, Http2Headers http2Headers, boolean b) {
                        ALog.d(TAG, "onHeadersRead() called with: s = [" + s + "], http2Headers = [" + http2Headers + "], b = [" + b + "]");
                        unlimitedStreamId = 0;
                        ALog.d(TAG, "receive headers=" + http2Headers );
                    }

                    @Override
                    public void onDataRead(String s, byte[] bytes, boolean b) {
                        unlimitedStreamId = 0;
                        ALog.d(TAG, "onDataRead() called with: s = [" + s + "], bytes = [" + new String(bytes) + "], b = [" + b + "]");
                    }

                    @Override
                    public void onStreamError(String s, IOException e) {
                        unlimitedStreamId = 0;
                        ALog.w(TAG, "onStreamError() called with: s = [" + s + "], e = [" + e + "]");
                    }
                }, new CompletableListener<StreamWriteContext>() {
                    @Override
                    public void complete(StreamWriteContext streamWriteContext) {
                        ALog.d(TAG, "complete() called with: streamWriteContext = [" + streamWriteContext + "]");
                        if (type == TYPE_UNLIMITED_REQUEST && streamWriteContext != null) {
                            unlimitedStreamId = streamWriteContext.getStream().id();
                            ALog.i(TAG, "unlimitedStreamId=" + unlimitedStreamId);
                        }
                    }

                    @Override
                    public void completeExceptionally(Throwable throwable) {
                        unlimitedStreamId = 0;
                        ALog.w(TAG, "completeExceptionally() called with: throwable = [" + throwable + "]");
                    }
                });
    }

    private static String toHexString(byte[] bytes) {
        Formatter formatter = new Formatter();
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }

        String res = formatter.toString();
        formatter.close();

        return res;
    }

    private String getMd5Value(String filePath) {

        String md5Value = null;
        try {
            byte[] buffer = new byte[8192];
            int len = 0;
            MessageDigest md = MessageDigest.getInstance("MD5");
            File f = new File(filePath);
            FileInputStream fis = new FileInputStream(f);
            while ((len = fis.read(buffer)) != -1) {
                md.update(buffer, 0, len);
            }
            fis.close();
            byte[] b = md.digest();
            md5Value = toHexString(b);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return md5Value;
    }

    public void sendFile(String filePath,final  CompletableListener<Http2Response> completableListener){
        if (client == null) {
            ALog.e(TAG, "open stream first.");
            return;
        }
        ALog.d(TAG, "filePath = " + filePath);
        Http2Request request = new Http2Request();
        request.getHeaders().add("filename", filePath.substring(filePath.lastIndexOf('/') + 1));
        String md5Value = getMd5Value(filePath);
        request.getHeaders().add("x-send-md5", md5Value);
        Log.i(TAG, "x-send-md5:" + md5Value);
        client.upload(dataStreamId, filePath, request, new CompletableListener<Http2Response>() {
            @Override
            public void complete(Http2Response o) {
                ALog.d(TAG, "complete() called with: o = [" + o + "]");
                ALog.d(TAG, "upload success");
                completableListener.complete(o);
            }

            @Override
            public void completeExceptionally(Throwable throwable) {
                ALog.w(TAG, "completeExceptionally() called with: throwable = [" + throwable + "]");
                ALog.e(TAG, "upload fail");
                completableListener.completeExceptionally(throwable);
            }
        });
    }

    protected void closeStream() {
        ALog.i(TAG, "close stream");
        if (client == null || dataStreamOpened.compareAndSet(false, false)) {
            ALog.i(TAG, "open Stream first.");
            return;
        }
        Http2Request request = new Http2Request();
        request.setEndOfStream(true);
        request.getHeaders().add("x-for-test", "ddd");
        client.closeStream(dataStreamId, request, new CompletableListener<Http2Response>() {
            @Override
            public void complete(Http2Response http2Response) {
                ALog.d(TAG, "complete() called with: http2Response = [" + http2Response + "]");
                ALog.i(TAG, "close stream success");
                dataStreamId = null;
                unlimitedStreamId = 0;
                dataStreamOpened.set(false);
            }

            @Override
            public void completeExceptionally(Throwable throwable) {
                ALog.d(TAG, "completeExceptionally() called with: throwable = [" + throwable + "]");
                ALog.i(TAG, "close stream exception");
                dataStreamId = null;
                dataStreamOpened.set(false);
                unlimitedStreamId = 0;
            }
        });
    }

    protected void disconnect(final CompletableListener listener) {
        ALog.d(TAG, "disconnect");
        if (client != null) {
            client.disconnect(new CompletableListener<Object>(){

                @Override
                public void complete(Object o) {
                    disconnected();
                    if (listener != null) {
                        listener.complete(o);
                    }
                }

                @Override
                public void completeExceptionally(Throwable throwable) {
                    disconnected();
                    if (listener != null) {
                        listener.completeExceptionally(throwable);
                    }
                }
            });
        }

    }

    protected void disconnected(){
        ALog.i(TAG, "disconnect success");

        unlimitedStreamId = 0;
        dataStreamId = null;
        dataStreamOpened.set(false);
        client = null;

    }
}
