package com.adacampus.classbrand.faceverifyer.face;

import java.io.Serializable;

public class Face implements Serializable {
    public String id = "";
    public String face_hash = "";
    public String face_ident = "";
    public String image_url = "";


    /**
     * 截取身份识别前序，verifySdk对完整长度的face_ident对报错
     *
     * @return
     */
    public String getFixFaceIdent() {
        if (face_ident.contains("student-")) {
            return face_ident.replaceFirst("student-", "s-");
        } else if (face_ident.contains("teacher-")) {
            return face_ident.replaceFirst("teacher-", "t-");
        }

        return "";
    }

    public static String getOriginFaceIdent(String face_ident) {
        if (face_ident.contains("s-")) {
            return face_ident.replaceFirst("s-", "student-");
        } else if (face_ident.contains("t-")) {
            return face_ident.replaceFirst("t-", "teacher-");
        }

        return "";
    }

    @Override
    public String toString() {
        return "Face{" +
                "id='" + id + '\'' +
                ", face_hash='" + face_hash + '\'' +
                ", face_ident='" + face_ident + '\'' +
                ", image_url='" + image_url + '\'' +
                '}';
    }
}


