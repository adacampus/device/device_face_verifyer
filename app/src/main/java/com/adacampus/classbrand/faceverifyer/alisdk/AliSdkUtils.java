package com.adacampus.classbrand.faceverifyer.alisdk;


import com.adacampus.classbrand.library.utils.AdaLog;
import com.adacampus.classbrand.library.utils.FilePath;
import com.adacampus.classbrand.library.utils.ListUtils;
import com.adacampus.classbrand.faceverifyer.utils.Key;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.security.rp.verifysdk.FaceInfo;
import com.alibaba.security.rp.verifysdk.FaceMatchResult;
import com.alibaba.security.rp.verifysdk.FaceRect;
import com.alibaba.security.rp.verifysdk.LicenseExpireDate;
import com.alibaba.security.rp.verifysdk.MatchItem;
import com.alibaba.security.rp.verifysdk.VerifyFaceEngine;
import com.aliyun.alink.dm.api.InitResult;
import com.aliyun.alink.linksdk.cmp.connect.channel.MqttPublishRequest;
import com.aliyun.alink.linksdk.cmp.core.base.AMessage;
import com.aliyun.alink.linksdk.cmp.core.base.ARequest;
import com.aliyun.alink.linksdk.tools.AError;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AliSdkUtils {


    public static String getCurFacesJsonPath() {
        String path = FilePath.getStoragePath() + File.separator + "CurrentFaces.json";
        return path;
    }

    public static String getPropertyFilePath() {
        return FilePath.getStoragePath() + File.separator + "Property";
    }

    public static String getVerifyPath(){
        return FilePath.getStoragePath() + File.separator + "verify";
    }




    /**
     * 不存在文件则创建
     *
     * @param file
     */
    public static void createFile(File file) {
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return;
    }

    public static void createFile(String path) {
        File file = new File(path);
        createFile(file);
    }

    public static int getAmsgCode(AMessage aMessage) {
        String data = aMsgToString(aMessage);
        JSONObject payload = JSON.parseObject(data);
        return payload.getIntValue("code");
    }

    public static String aMsgToString(AMessage aMessage) {
        String data = new String((byte[]) aMessage.data);
        return data;
    }

    public static String aErrToString(AError aError) {
        StringBuffer sb = new StringBuffer();
        if (aError == null) {
            return sb.append("null").toString();
        }
        sb.append(" msg:" + aError.getMsg());
        sb.append(" code:" + aError.getCode());
        sb.append(" domain:" + aError.getDomain());
        sb.append(" subMsg:" + aError.getSubMsg());
        sb.append(" subCode:" + aError.getSubCode());
        sb.append(" subDomain:" + aError.getSubDomain());
        return sb.toString();
    }

    public static String faceInfoToString(FaceInfo faceInfo) {
        StringBuffer sb = new StringBuffer();
        if (faceInfo == null) {
            sb.append("null");
            return sb.toString();
        }
        sb.append("faceInfo.getErrorCode:" + faceInfo.getErrorCode());
        int size = ListUtils.isEmpty(faceInfo.getDetectedFaces()) ? 0 : faceInfo.getDetectedFaces().size();
        for (int i = 0; i < size; i++) {
            FaceRect faceRect = faceInfo.getDetectedFaces().get(i);
            sb.append("\nfaceRect:" + i
                    + "  faceRect.left:" + faceRect.getLeft()
                    + " faceRect.getWidth:" + faceRect.getWidth()
                    + " faceRect.getTop:" + faceRect.getTop()
                    + " faceRect.getHeight:" + faceRect.getHeight());
        }
        return sb.toString();
    }


    public static String faceQualityStateToString(VerifyFaceEngine.FaceQualityState faceQualityState) {
        StringBuffer sb = new StringBuffer();
        if (faceQualityState == null) {
            sb.append("null");
            return sb.toString();
        }


        if (faceQualityState.bFaceGood) {
            sb.append("bFaceGood:" + faceQualityState.bFaceGood + " -> 质量达标:是");
        } else {
            sb.append("bFaceGood:" + faceQualityState.bFaceGood + " -> 质量达标:否");
        }

        sb.append("\n");
        if (faceQualityState.bEyeClose) {
            sb.append("bEyeClose:" + faceQualityState.bEyeClose + " -> 闭眼:是");
        } else {
            sb.append("bEyeClose:" + faceQualityState.bEyeClose + "-> 闭眼:否");
        }

        sb.append("\n");
        if (faceQualityState.bBlur) {
            sb.append("bBlur:" + faceQualityState.bBlur + " -> 模糊不清:是");
        } else {
            sb.append("bBlur:" + faceQualityState.bBlur + " -> 模糊不清:否");
        }

        sb.append("\n");
        if (faceQualityState.bMouthOpen) {
            sb.append("bMouthOpen:" + faceQualityState.bMouthOpen + " -> 张嘴:是");
        } else {
            sb.append("bMouthOpen:" + faceQualityState.bMouthOpen + " - > 张嘴:否");
        }

        sb.append("\n");
        if (faceQualityState.bMotion) {
            sb.append("bMotion:" + faceQualityState.bMotion + " -> 移动:是");
        } else {
            sb.append("bMotion:" + faceQualityState.bMotion + " -> 移动:否");
        }

        sb.append("\n");
        if (faceQualityState.bOutOfRegion) {
            sb.append("bOutOfRegion:" + faceQualityState.bOutOfRegion + " -> 越界:是");
        } else {
            sb.append("bOutOfRegion:" + faceQualityState.bOutOfRegion + " -> 越界:否");
        }

        sb.append("\n");
        if (faceQualityState.bTooBright) {
            sb.append("bTooBright:" + faceQualityState.bTooBright + " -> 过亮:是");
        } else {
            sb.append("bTooBright:" + faceQualityState.bTooBright + " -> 过亮:否");
        }

        sb.append("\n");
        if (faceQualityState.bTooDark) {
            sb.append("bTooDark:" + faceQualityState.bTooDark + " -> 太暗:是");
        } else {
            sb.append("bTooDark:" + faceQualityState.bTooDark + " -> 太暗:否");
        }

        sb.append("\n");
        if (faceQualityState.bTooSmall) {
            sb.append("bTooSmall:" + faceQualityState.bTooSmall + " -> 太小:是");
        } else {
            sb.append("bTooSmall:" + faceQualityState.bTooSmall + " -> 太小:否");
        }

        sb.append("\n");
        if (faceQualityState.bYawPose) {
            sb.append("bYawPose:" + faceQualityState.bYawPose + " -> 侧脸过大:是");
        } else {
            sb.append("bYawPose:" + faceQualityState.bYawPose + " -> 侧脸过大:否");
        }

        sb.append("\n");
        if (faceQualityState.bPitchPose) {
            sb.append("bPitchPose:" + faceQualityState.bPitchPose + " -> 俯仰过大:是");
        } else {
            sb.append("bPitchPose:" + faceQualityState.bPitchPose + " -> 俯仰过大:否");
        }

        sb.append("\n");
        if (faceQualityState.bUneven) {
            sb.append("bUneven:" + faceQualityState.bUneven + " -> 光照不均匀:是");
        } else {
            sb.append("bUneven:" + faceQualityState.bUneven + " -> 光照不均匀:否");
        }
        sb.append("\n");
        sb.append("张嘴得分(0~1.0):" + faceQualityState.mouthOpenValue);
        sb.append("\n");
        sb.append("移动得分(pixel per second * 0.1f):" + faceQualityState.motionSpeed);
        sb.append("\n");
        sb.append("闭眼得分(0~1.0):" + faceQualityState.eyeCloseValue);
        sb.append("\n");
        sb.append("亮度得分(0~255):" + faceQualityState.brightness);
        sb.append("\n");
        sb.append("亮度不均得分(0~100):" + faceQualityState.brightnessDiff);
        sb.append("\n");
        sb.append("锐度得分(0~1.0):" + faceQualityState.sharpnessScore);
        sb.append("\n");
        sb.append("人脸左右偏离角度(-0.5~0.5):" + faceQualityState.yawValue);
        sb.append("\n");
        sb.append("人脸俯仰角度(-0.5~0.5):" + faceQualityState.pitchValue);
        return sb.toString();

    }

    public static String faceMatchResultToString(FaceMatchResult matchResult) {
        StringBuffer sb = new StringBuffer();
        if (matchResult == null) {
            sb.append("null");
            return sb.toString();
        }

        sb.append(" errorcode:" + matchResult.getErrorCode()
                + " matchedNum." + matchResult.getMatchedNum()
                + " getMsgType" + matchResult.getMsgType()
                + " getFaceRect" + matchResult.getFaceRect()
                + " faceRect.left:" + matchResult.getFaceRect().getLeft()
                + " faceRect.getWidth:" + matchResult.getFaceRect().getWidth()
                + " faceRect.getTop:" + matchResult.getFaceRect().getTop()
                + " faceRect.getHeight:" + matchResult.getFaceRect().getHeight());

        int size = ListUtils.isEmpty(matchResult.getMatchItems()) ? 0 : matchResult.getMatchItems().size();
        for (int i = 0; i < size; i++) {
            MatchItem item = matchResult.getMatchItems().get(i);
            sb.append("\nMatchItem:" + i
                    + "  item.getUserID:" + item.getUserID()
                    + " item.getProperties:" + item.getProperties()
                    + " item.getScore:" + item.getScore());
        }
        return sb.toString();
    }

    public static String aRequestToString(ARequest aRequest) {
        StringBuffer sb = new StringBuffer();
        if (aRequest instanceof MqttPublishRequest) {
            MqttPublishRequest mqttPublishRequest = (MqttPublishRequest) aRequest;
            sb.append("msgId").append(mqttPublishRequest.msgId).append(" ");
            sb.append("isRPC:").append(mqttPublishRequest.isRPC).append(" ");
            sb.append("qos:").append(mqttPublishRequest.qos).append(" ");
            sb.append("replyTopic:").append(mqttPublishRequest.replyTopic).append(" ");
            sb.append("payloadObj:").append(mqttPublishRequest.payloadObj);

        }
        return sb.toString();
    }


    public static String objToString(Object obj) {
        StringBuffer sb = new StringBuffer();
        if (obj instanceof InitResult) {
            InitResult initResult = (InitResult) obj;
            sb.append("tls:").append(initResult.tsl);
        }
        return sb.toString();
    }


    public static String parseIdentifierFromMsg(AMessage aMessage) {

        String serviceIdentifier = null;

        if (aMessage != null) {
            String data = new String((byte[]) aMessage.data);
            com.alibaba.fastjson.JSONObject obj = JSON.parseObject(data);
            serviceIdentifier = obj.getString("method").substring(obj.getString("method").lastIndexOf('.') + 1);
        }

        return serviceIdentifier;
    }


    /**
     * @return
     * @desc 从aMessage中解析params描述符列表
     */
    public static List<String> parseParamNamesFromMsg(AMessage aMessage) {
        List<String> strings = new ArrayList<>();
        if (null == aMessage) {
            return strings;
        }
        String data = new String((byte[]) aMessage.data);
        com.alibaba.fastjson.JSONObject obj = JSON.parseObject(data);
        if (null == obj.getString("params")) {
            return strings;
        }
        for (Map.Entry<String, Object> entry : obj.getJSONObject("params").entrySet()) {
            strings.add(entry.getKey());
        }
        return strings;
    }


    /**
     * 获取指定参数名的值
     *
     * @param aMessage
     * @param paramName
     * @return
     */
    public static String parseParamValueFromMsg(AMessage aMessage, String paramName) {
        String paramValue = null;
        if (aMessage != null) {
            String data = new String((byte[]) aMessage.data);
            com.alibaba.fastjson.JSONObject obj = JSON.parseObject(data);
            if (null != obj.getString("params")) {
                paramValue = obj.getJSONObject("params").getString(paramName);
            }
        }
        return paramValue;
    }

    public void checkLicenseExpireDate(LicenseExpireDate expireDate) {
        try {
            //格式：1970到现在的天数
            long begin = expireDate.getBeginDate() * 3600 * 24 * 1000L;
            long end = expireDate.getEndDate() * 3600 * 24 * 1000L;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            AdaLog.d(Key.LOG, AliSdkManager.Tag, "checkLicenseExpireDate", "license start date:" + sdf.format(begin));
            AdaLog.d(Key.LOG, AliSdkManager.Tag, "checkLicenseExpireDate", "license end date:" + sdf.format(end));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
