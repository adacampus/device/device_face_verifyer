package com.adacampus.classbrand.faceverifyer.face.report;

import com.adacampus.classbrand.faceverifyer.utils.Key;
import com.adacampus.classbrand.library.net.api.ErrorResponse;
import com.adacampus.classbrand.library.net.api.ResultCallback;
import com.adacampus.classbrand.faceverifyer.net.CommonApiServer;
import com.adacampus.classbrand.faceverifyer.net.DataLoader;
import com.adacampus.classbrand.faceverifyer.net.SysRetrofitHelper;
import com.adacampus.classbrand.faceverifyer.utils.MsgEvent;
import com.adacampus.classbrand.library.utils.AdaLog;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;

public class FaceVerifyReportLoader<T> extends DataLoader<T> {

    public static final String TAG = "FaceVerifyReportLoader";

    public Call<FaceReportResult> mCall;
    private String mUserID = "";
    private String mStatus = "";
    private String mSysBaseUrl = "";

    @Override
    public void loadData() {
        AdaLog.d(Key.LOG, TAG, "loadData", "开始上传审核结果:faceId:" + mUserID);
        EventBus.getDefault().post(new MsgEvent("开始上传审核结果:faceId:" + mUserID));
        mCall.enqueue(new ResultCallback<FaceReportResult>() {
            @Override
            public void onResponseSuccessful(FaceReportResult data) {
                AdaLog.d(Key.LOG, TAG, "onResponseSuccessful", "审核结果上传成功:" + data.result);
                EventBus.getDefault().post(new MsgEvent("审核结果上传:" + (data.result ? "成功" : "失败")));
                mData = (T) data;
                if (mDataLoaderCallBack != null) {
                    mDataLoaderCallBack.onDataUpdate(mData);
                }
            }

            @Override
            public void onResponseFailure(ErrorResponse errorResponse) {
                AdaLog.e(Key.LOG, TAG, "onResponseFailure", "errorResponse:" + errorResponse.toString());
                EventBus.getDefault().post(new MsgEvent("审核结果上传失败:终止上传:" + errorResponse.error_message));
                if (mDataLoadFailureCallBack != null) {
                    mDataLoadFailureCallBack.onDataLoadFailed();
                }
            }

            @Override
            public void onFailure(Call<FaceReportResult> call, Throwable t) {
                AdaLog.e(Key.LOG, TAG, "onFailure", "exception:" + t.getMessage());
                if (mDataLoadFailureCallBack != null) {
                    mDataLoadFailureCallBack.onDataLoadFailed();
                }
            }

            @Override
            public void onFinish(Call<FaceReportResult> call) {
                EventBus.getDefault().post(new MsgEvent("*********************************************************************************************"));
                cancel();
                reset();
            }
        });
    }

    @Override
    public void cancel() {
        if (mCall != null) {
            if (!mCall.isCanceled()) {
                mCall.cancel();
            }
        }
    }

    public void setParams(String user_id, String status, String sysBaseUrl) {
        mUserID = user_id;
        mStatus = status;
        mSysBaseUrl = sysBaseUrl;
    }

    @Override
    public void reset() {
        CommonApiServer apiServer = SysRetrofitHelper.getTargetSysRetrofit(mSysBaseUrl).create(CommonApiServer.class);
        mCall = apiServer.sendReport(mUserID, mStatus);
    }

}
