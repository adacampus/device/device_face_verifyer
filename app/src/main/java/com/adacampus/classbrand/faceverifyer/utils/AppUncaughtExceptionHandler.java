package com.adacampus.classbrand.faceverifyer.utils;


import com.adacampus.classbrand.library.base.BaseApp;
import com.adacampus.classbrand.library.utils.AppUtils;
import com.elvishew.xlog.XLog;

import java.io.PrintWriter;
import java.io.StringWriter;


/**
 * Created by jeffrey on 2017/3/16.
 */

public class AppUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        try {
            XLog.e("############################ UncaughtException Start ############################" + "\n" + "Thread :" + thread.toString() + "\n" + getStackTrace(ex) + "############################ UncaughtException End ############################");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            MobclickAgent.onKillProcess(BaseApp.getApp());
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000); //为保证bugly日志正常上传，睡眠一秒
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        AppUtils.stopApp(BaseApp.getApp().getMainAppId());
                    }

                }
            }).start();
        }
    }


    public String getStackTrace(Throwable throwable) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        throwable.printStackTrace(printWriter);
        printWriter.close();
        return stringWriter.toString();
    }

}
