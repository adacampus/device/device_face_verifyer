package com.adacampus.classbrand.faceverifyer.alisdk;

public class FaceDoorProperty {
    // 人脸比对成功阈值
    private Double OnMatchThreshOld;
    // 人脸库MD5签名
    private String FaceSetMD5Sign;
    // 人脸库ID
    private String FaceSetID;
    // 人脸组ID
    private String FaceGroupID;
    // 当前人脸库大小
    private int FaceSetSize;
    // 人脸库算法版本
    private String FaceSetAlgorithmVersion;
    // 人脸库图片存储能力
    private int FaceSetPicStoreAbility;
    // 端侧添加过的人脸信息文件名(包含添加成功、添加失败)
    private String AddedFaceSetName;


    public Double getOnMatchThreshOld() {
        return OnMatchThreshOld;
    }
    public String getFaceSetMD5Sign() {
        return FaceSetMD5Sign;
    }
    public String getFaceSetAlgorithmVersion() {
        return FaceSetAlgorithmVersion;
    }
    public String getFaceSetID() {
        return FaceSetID;
    }
    public String getFaceGroupIDID() {
        return FaceGroupID;
    }
    public String getAddedFaceSetName() {return AddedFaceSetName;}
    public int getFaceSetSize(){
        return FaceSetSize;
    }
    public int getFaceSetPicStoreAbility(){
        return FaceSetPicStoreAbility;
    }


    public void setOnMatchThreshOld(Double ThreshOld){
        OnMatchThreshOld = ThreshOld;
    }
    public void setFaceSetMD5Sign(String MD5Sign){
        FaceSetMD5Sign = MD5Sign;
    }
    public void setFaceSetID(String ID){
        FaceSetID = ID;
    }
    public void setFaceGroupIDID(String ID){
        FaceGroupID = ID;
    }
    public void setAddedFaceSetName(String name){ AddedFaceSetName = name;}
    public void setFaceSetSize(int Size){
        FaceSetSize = Size;
    }
    public void setFaceSetAlgorithmVersion(String AlgorithmVersion){
        FaceSetAlgorithmVersion = AlgorithmVersion;
    }
    public void setFaceSetPicStoreAbility(int Ability) {
        FaceSetPicStoreAbility = Ability;
    }
}
