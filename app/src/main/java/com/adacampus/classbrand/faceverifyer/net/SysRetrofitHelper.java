package com.adacampus.classbrand.faceverifyer.net;


import com.adacampus.classbrand.library.net.api.BaseAPI;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SysRetrofitHelper {

    private static SysRetrofitHelper mInstance;
    private Retrofit mRetrofit;


    private SysRetrofitHelper() {
    }

    public static SysRetrofitHelper getInstance() {
        if (mInstance == null) {
            synchronized (SysRetrofitHelper.class) {
                if (mInstance == null) {
                    mInstance = new SysRetrofitHelper();
                }
            }
        }
        return mInstance;
    }

    public void init(String baseUrl) {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(BaseAPI.getSystemClient()).build();
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    public static Retrofit getTargetSysRetrofit(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(BaseAPI.getSystemClient()).build();
        return retrofit;
    }


}
