package com.adacampus.classbrand.faceverifyer.alisdk;


import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * 人脸检测算法线程池创建类
 *
 * @author tingyi
 * @date 2018/10/24
 */
public class FaceDoorThreadPool {
    private ThreadPoolExecutor threadPool;

    private static FaceDoorThreadPool faceDoorThreadPool = new FaceDoorThreadPool();

    public static FaceDoorThreadPool getInstance() {
        return faceDoorThreadPool;
    }

    /**
     * 构造函数时，创建线程池
     */
    public FaceDoorThreadPool() {
        this.threadPool = new ThreadPoolExecutor(5, 10, 200, TimeUnit.MICROSECONDS, new ArrayBlockingQueue<Runnable>(5), new FaceDoorThreadFactory("AntVerification"));
    }

    public ThreadPoolExecutor getThreadPool() {
        return threadPool;
    }
}
