package com.adacampus.classbrand.faceverifyer.face.verify;


import android.content.Context;

import com.adacampus.classbrand.faceverifyer.alisdk.VerifySdkListener;
import com.adacampus.classbrand.faceverifyer.face.Face;
import com.adacampus.classbrand.faceverifyer.face.FaceImageDownloader;
import com.adacampus.classbrand.faceverifyer.face.report.FaceReportResult;
import com.adacampus.classbrand.faceverifyer.face.report.FaceVerifyReportLoader;
import com.adacampus.classbrand.library.net.mq.EventHandler;
import com.adacampus.classbrand.library.net.mq.IEventHandler;
import com.adacampus.classbrand.library.utils.AdaLog;
import com.adacampus.classbrand.faceverifyer.net.DataLoader;
import com.adacampus.classbrand.faceverifyer.utils.MsgEvent;
import com.adacampus.classbrand.faceverifyer.utils.Key;
import com.alibaba.security.rp.verifysdk.service.VerifySDKManager;
import com.alibaba.security.rp.verifysdk.util.Type;
import com.lzy.okgo.model.Progress;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Envelope;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FaceDataVerifyer implements IEventHandler, FaceImageDownloader.OnFaceImageDownloadCallback {

    private FaceImageDownloader mDownloader;
    private FaceDataVerifyerEventHandler mEvntHandler;
    private SingleFaceLoader<Face> mSingleFaceLoader;
    private FaceVerifyReportLoader<FaceReportResult> mFaceReportLoader;
    private String mFaceSystBaseUrl = "";
    private String curFaceImageId;
    private Context mContext;
    private static final String Tag = "FaceDataVerifyer";

    private class FaceDataVerifyerEventHandler extends EventHandler {

        public FaceDataVerifyerEventHandler() {
            mEventKey = Key.KEY_EVENT_FACE_VERIFICATION;
        }


        @Override
        public void handleEvent(JSONObject json, Channel channel, Envelope envelope) {
            EventBus.getDefault().post(new MsgEvent("*********************************************************************************************"));
            EventBus.getDefault().post(new MsgEvent("接收推送:" + Key.KEY_EVENT_FACE_VERIFICATION));
            try {
                String faceid = json.getString("face_id");
                String systemApiEndPoint = json.getString("system_api_endpoint");
                EventBus.getDefault().post(new MsgEvent("人脸信息faceId:" + faceid));
                loadImageWithFaceId(faceid, systemApiEndPoint);
            } catch (Exception e) {
                e.printStackTrace();
                AdaLog.e(Key.LOG, Tag, "FaceDataVerifyerEventHandler.handleEvent", "exception:" + e.getMessage());
                EventBus.getDefault().post(new MsgEvent(e.getMessage()));
                ack();
            }
        }
    }

    private void loadImageWithFaceId(String faceId, String url) {
        mFaceSystBaseUrl = url;
        mSingleFaceLoader.cancel();
        curFaceImageId = faceId;
        mSingleFaceLoader.setParams(faceId, mFaceSystBaseUrl);
        mSingleFaceLoader.reset();
        mSingleFaceLoader.loadData();
    }

    @Override
    public EventHandler getEventHandler() {
        return mEvntHandler;
    }

    public FaceDataVerifyer() {
        mSingleFaceLoader = new SingleFaceLoader<>();
        mSingleFaceLoader.setDataLoaderCallBack(new DataLoader.DataLoaderCallBack<Face>() {
            @Override
            public void onDataUpdate(Face face) {
                mDownloader.download(face);

            }
        });
        mSingleFaceLoader.setDataLoadFailureCallBack(new DataLoader.DataLoadFailureCallBack() {
            @Override
            public void onDataLoadFailed() {
                reportFaceState(false);
            }
        });

        mFaceReportLoader = new FaceVerifyReportLoader<>();
        mFaceReportLoader.setDataLoaderCallBack(new DataLoader.DataLoaderCallBack<FaceReportResult>() {
            @Override
            public void onDataUpdate(FaceReportResult data) {
                mEvntHandler.ack();
            }
        });
        mFaceReportLoader.setDataLoadFailureCallBack(new DataLoader.DataLoadFailureCallBack() {
            @Override
            public void onDataLoadFailed() {
                mEvntHandler.ack();
            }
        });

        mDownloader = new FaceImageDownloader(mContext);
        mDownloader.setFaceImageDownloadCallback(this);
        mEvntHandler = new FaceDataVerifyerEventHandler();

    }


    @Override
    public void onLoadFaceImageFailure(Face face, Progress progress) {
        // Progress{fraction=0.0, totalSize=-1, currentSize=0, speed=0, status=4, priority=2147483647,
        EventBus.getDefault().post(new MsgEvent("下载失败,终止当前照片下载"));
        reportFaceState(false);
    }

    @Override
    public void onLoadFaceImageSuccessful(Face face, File file) {
        EventBus.getDefault().post(new MsgEvent("下载完成:开始使用sdk审核照片"));
        final byte[] imgBytes = readImageFile(file);
//        byte[] zipData = zip(imgBytes);
        boolean temp = file.delete();
        EventBus.getDefault().post(new MsgEvent("删除本地照片结果:" + (temp ? "成功" : "失败")));
        VerifySDKManager.getInstance().addUser(true, face.getFixFaceIdent(), Type.BIOLOGY_FACE, imgBytes, new AddFaceListener(face));
    }


    private class DelFaceListener extends VerifySdkListener {
        @Override
        public void onSingleUserLibUpdate(String face_ident, int errorCode) {
            AdaLog.d(Key.LOG, DelFaceListener.this, "onSingleUserLibUpdate", "face_ident:" + face_ident + " errorCode:" + errorCode);
            EventBus.getDefault().post(new MsgEvent(String.format("删除sdk内部人脸结果:" + Face.getOriginFaceIdent(face_ident) + " errorCode " + errorCode + "  " + (errorCode == 0 ? "成功" : "失败"))));
            if (errorCode == 0) {
            }
        }
    }


    private class AddFaceListener extends VerifySdkListener {
        private Face mFace;

        public AddFaceListener(Face face) {
            mFace = face;
        }

        @Override
        public void onSingleUserLibUpdate(String face_ident, int errorCode) {
            EventBus.getDefault().post(new MsgEvent(String.format("使用sdk审核人脸结果:" + Face.getOriginFaceIdent(face_ident) + " errorCode " + errorCode + "  " + (errorCode == 0 ? "成功" : "失败"))));
            AdaLog.d(Key.LOG, AddFaceListener.this, "onSingleUserLibUpdate", "face_ident:" + Face.getOriginFaceIdent(face_ident) + " errorCode:" + errorCode);
            if (mFace.getFixFaceIdent().equals(face_ident) && errorCode == 0) {
                reportFaceState(true);
                VerifySDKManager.getInstance().removeUser(true, face_ident, new DelFaceListener());
            } else {
                reportFaceState(false);
            }
        }
    }

    private void reportFaceState(boolean verified) {
        if (verified) {
            mFaceReportLoader.cancel();
            mFaceReportLoader.setParams(curFaceImageId, "verified", mFaceSystBaseUrl);
            mFaceReportLoader.reset();
            mFaceReportLoader.loadData();
        } else {
            mFaceReportLoader.cancel();
            mFaceReportLoader.setParams(curFaceImageId, "rejected", mFaceSystBaseUrl);
            mFaceReportLoader.reset();
            mFaceReportLoader.loadData();
        }
    }

    //压缩图片数据
    public byte[] zip(byte[] data) {
        byte[] b = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ZipOutputStream zip = new ZipOutputStream(bos);
            ZipEntry entry = new ZipEntry("~~~1.bmp");
            entry.setSize(data.length);//返回条目数据的未压缩大小；如果未知，则返回 -1。
            zip.putNextEntry(entry);// 开始写入新的 ZIP 文件条目并将流定位到条目数据的开始处
            zip.write(data);//将字节数组写入当前 ZIP 条目数据。
            zip.closeEntry();
            zip.close();
            b = bos.toByteArray();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return b;
    }

    private byte[] readImageFile(File file) {
        try {
            InputStream is = new FileInputStream(file);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024 * 10];
            int n = 0;
            while ((n = is.read(buffer)) != -1) {
                out.write(buffer, 0, n);
            }
            return out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }
}
