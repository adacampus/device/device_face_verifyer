package com.adacampus.classbrand.faceverifyer.face;



import com.adacampus.classbrand.faceverifyer.net.BaseDataController;
import com.adacampus.classbrand.faceverifyer.net.DataLoader;
import com.adacampus.classbrand.faceverifyer.net.ListDataParser;

import java.util.List;

public class FaceListDataController extends BaseDataController implements DataLoader.DataLoaderCallBack<List<Face>>, DataLoader.DataLoadFailureCallBack {

    public ListDataParser<Face> mListDataParser;

    public FaceListDataController(ListDataParser<Face> listDataParser, FaceListDataLoader<List<Face>> dataLoader) {
        mListDataParser = listDataParser;
        mDataLoader = dataLoader;
        mDataLoader.setDataLoaderCallBack(this);
        mDataLoader.setDataLoadFailureCallBack(this);
    }

    @Override
    public void onDataUpdate(List<Face> list) {
        mListDataParser.setData(list);
        if (mUpdateCallback != null) {
            mUpdateCallback.onRefresh();
        }
    }


    @Override
    public void onDataLoadFailed() {

    }

}
