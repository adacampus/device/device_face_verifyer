package com.adacampus.classbrand.faceverifyer.net;

import com.adacampus.classbrand.faceverifyer.face.Face;
import com.adacampus.classbrand.faceverifyer.face.report.FaceReportResult;
import com.adacampus.classbrand.library.net.api.EmptyResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CommonApiServer {


    @GET("/device/common/faces")
    Call<List<Face>> getFaceList();


    @POST("/device/common/faces/{id}/report")
    Call<FaceReportResult> sendReport(@Path("id") String user_id, @Query("status") String status);

    @POST("/device/common/faces/report_face_set")
    Call<EmptyResult> postFaceCountReport(@Query("faces_count") long faces_count);

    @GET("/device/common/faces/{id}")
    Call<Face> getFaceByUserId(@Path("id") String user_id);
}
