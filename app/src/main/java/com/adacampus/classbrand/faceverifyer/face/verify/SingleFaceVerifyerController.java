package com.adacampus.classbrand.faceverifyer.face.verify;


import com.adacampus.classbrand.faceverifyer.face.Face;
import com.adacampus.classbrand.faceverifyer.net.BaseDataController;
import com.adacampus.classbrand.faceverifyer.net.DataLoader;

import java.util.List;

public class SingleFaceVerifyerController extends BaseDataController implements DataLoader.DataLoaderCallBack<List<Face>>, DataLoader.DataLoadFailureCallBack {


    public SingleFaceVerifyerController(SingleFaceLoader<Face> dataLoader) {
        mDataLoader = dataLoader;
        mDataLoader.setDataLoaderCallBack(this);
        mDataLoader.setDataLoadFailureCallBack(this);
    }

    @Override
    public void onDataUpdate(List<Face> list) {
        if (mUpdateCallback != null) {
            mUpdateCallback.onRefresh();
        }
    }


    @Override
    public void onDataLoadFailed() {

    }

}
