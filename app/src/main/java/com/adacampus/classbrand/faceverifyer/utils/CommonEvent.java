package com.adacampus.classbrand.faceverifyer.utils;

public class CommonEvent {

    public static final String TYPE_ADD_CHANGE = "TYPE_ADD_CHANGE";
    public static final String TYPE_QUANTITY = "TYPE_QUANTITY";
    public static final String TYPE_DEL_CHANGE = "TYPE_DEL_CHANGE";
    public static final String TYPE_SYNC_COUNT = "TYPE_SYNC_COUNT";
    public static final String TYPE_UPDATE_COUNT = "TYPE_UPDATE_COUNT";


    public String mType;
    public String mMsg;

    public CommonEvent(String type, String msg) {
        mType = type;
        mMsg = msg;
    }
}
