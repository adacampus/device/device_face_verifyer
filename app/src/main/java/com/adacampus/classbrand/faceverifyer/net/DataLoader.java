package com.adacampus.classbrand.faceverifyer.net;



public abstract class DataLoader<T> {

    protected T mData;
    public boolean mLoading = false;

    public DataLoaderCallBack mDataLoaderCallBack;
    public DataLoadFailureCallBack mDataLoadFailureCallBack;
    public DataLoadCancelCallback mCancelCallback;

    public T getData() {
        return mData;
    }


    public abstract void loadData();

    public void cancel() {

    }

    public void reset() {

    }

    public void loadNextPage() {

    }

    public void loadPrevPage() {

    }

    /**
     * 当设置回调时，如果已有旧数据则马上回调一次更新，场景:重新加载了卡片使用马上是使用原数据。
     *
     * @param callback
     */
    public void setDataLoaderCallBack(DataLoaderCallBack callback) {
        if (mData != null && mDataLoaderCallBack == null) {
            callback.onDataUpdate(mData);
        }
        mDataLoaderCallBack = callback;

    }

    public void setDataLoadFailureCallBack(DataLoadFailureCallBack callBack) {
        mDataLoadFailureCallBack = callBack;
    }


    public interface DataLoaderCallBack<T> {

        public void onDataUpdate(T data);

    }

    public interface DataLoadFailureCallBack {

        public void onDataLoadFailed();
    }


    public void setCancelCallback(DataLoadCancelCallback cancelCallback) {
        mCancelCallback = cancelCallback;
    }

    public interface DataLoadCancelCallback {
        public void onDataLoadCancel();
    }

}
